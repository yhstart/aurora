# suroong(aurora 0.5.x 分支版本)
- php框架 ThinkPHP5.1.x
- 2018年1月2日 星期二
- Joshua Conero
- 版本信息

# V6
> <span style="color:red;">*当前项目的最后一个版本，主要为修复版系统*</span>



## V6.1

### todos

- _**WAP**_
  - *删除，edit等多页面；使用单页面改造 (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)*
  - **faccount**
    - *index*
      - 分组查询的搜索意义不明确 (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)
    - *edit*
      - 模板新增时，可选择是否明细同步复制生成 (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)
- **数据库**
  - `fnc1000c` 添加字符方式 `pay_method`，  (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)
  - `sys_user` 新增有效标志 `status` (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)
- **系统**
  - `注册必须输入邮箱验证码`  (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)
  - *用户登录系统时远程服务请求网络失败导致登录缓慢的问题*  (<span style="font-size:1.23em;color: blue;">NeedToDo</span>)



### v6.1.0/next

- *model*
  - User
    - (+) *新增用户状态相关的常量信息*
    - (+) *数据库新增字段 `last_lgtime` 最新登录时间，用于替换判断原 `mtime` 字段*
- *wap*
  - (实现) wap/verify
    - 新增移动端的用户验证界面，用于实现用户有效性验证
  - wap/faccount
    - (优化) *edit 标签优化，增强用户体验度（历史难以删除标签）*
    - (调整) *账单明细表更新，添加更多的字段*
    - (优化) *账单明细优化，以及实现名称等待自动完成*
  - wap/faffair
    - (优化) *edit 中分组吧标志SQL优化，使之满足 `only_full_group_by` 模式*
- *API*
  - (实现) *api/verify*
    - 新增接口实现用户邮箱验证码认证，支持wap/pc
- *asset*
  - (实现) 修改项目使之支持 `ts` 脚本
- *其他*
  - (实现) *邮箱验证码发送实现，根据配置的模板*
  - (实现) *用户登录检测，即错误的尝试登录规定次数如`10`次应用将被锁定(基于session的实现)规定时长(30分钟)*
  - (优化) 注销登录方法公有化，即支持其他程序的调用(`Aurora::loginExit`)。




## V6.0

### [WAP]

- *为一致性，点击页脚即版本栏时可加载更多数据*



> `依赖`

- thinkPHP *v5.1.37；固定依赖框架的版本，发现考虑代码升级的兼容性*

### 6.0.3/190811

> 实现

- *service*
  - (+) *新增服务目录，用于编写系统通用的服务类*
  - (+) *RequestBeforeCheck*
    - 实现入口检测 *`enter`* ，用于全站初始化处理，类似中间件。其通过 `app.only_site_user` 配置项进行开关
- 其他程序
  - (+) *新增 【EnumBase】 类使用实现基本的服务端常量*
  - *`app\common\controller\Api`*
    - (优化) *FeekMsg 方法做增强优化，并使用 EnumBase 常量改写返回的code，以及逻辑做增强优化*
- Wap
  - Faccount
    - (修复) *账单删除因为 tag 的模式更改，造成删除是时tag 同步删除错误*
  - (优化) *编辑页面(edit) 新增账单时做表单重复性检测，并提示*
- 依赖
  - *weui*
    - (优化) *升级 v2.0.1 版本后许多样式出现变化，无法与 weui.js 配置使用，代码不稳定。版本回滚到 v1.1.3*



### 6.0.2/190724

> **【实现】**

- API
  -  faccount
    - (+) sets *添加对条件筛选功能*
    - (优化) *save 支持新版的标签数据保护*
  - finance
    - (优化) *get_tags 接口添加可筛选的 tag 参数，用于模糊查询；以及优化查询SQL*
- Wap
  - (修复) *新版样式中，weui-footer 会遮蔽底层一些列表*
  - Faccout
    - (+) *index： 条件搜索添加 tag ，支持对账单多字段查询。以及后台接口统一使用 `faccount/sets`*
    - (优化) index: *条件搜索支持 `clob` 查询*
- *model: Fnc1000c*
  - (+) *添加方法 `setWhere` 用于设置条件查询*
  - (+) *新增属性 `$_tagName` 用于实现从条件中解出 tag 搜索，并实现对新版tag的搜索*
  - (+) *`Fnc1000c: getFinanceSets/getFinanceSetsGroup`  方法添加支持 setWhere 函数传递的where条件*
  - (+) *新增的 Tag 的数据处理 `getTags/tagSave`， 保存和获取*
  - (修复) *加载更多时，没有数据条件下会在重复在div尾部添加重复的内容*
  - (优化) *数据查询/生成单独的主键时，同步设置当前模型的实例*
  - (优化) *`getFinanceSets` 支持对新标签的支持*
- **assets**
  - Wap.js
    - (+) *实现移动端页面的 tag 标签，样式使用 ant-mobile中的 tag标签*
    - (+) *新增方法 `weuiLoading` 用于配置 ApiRequest(Aruora.js) 的超时处理，以及将文件中的 weui.loading 进行替换*
  - *Aruora.js*
    - (优化) *ApiRequest 方法尝试引入超时处理，既不会因为ajax错误而卡死 (`实现`)*
- **数据库设计**
  - (+) *visit 数据表新增自动，URL, REFERER, SESSION_ID 等；以及数据新增时实现*



> **[修复]**

- *类 extend/hyang/HRedis 在PHP7.3中 Redis 类不存在抛出异常无效*



> **【升级】**

- wap 版中升级： `weui/weui.js` 等项目依赖
  - weui: v2.0.1   (20190723)




### 6.0.1/190216

> **[修复]**

- *api: lgrecord/data* 框架升级操作的错误修复



### 6.0.0/190209

> **【新增】**

* 添加新的公共函数 *debugs*，用于调试多变量打印

> **【调整】**

- *thinkPHP 框架升级至 v5.1.34*

> **【修复】**

- *api: faccount/detailSync*  框架升级引起的SQL生成器错误，改写SQL生成器
- *wap: faffair* *无法加载更多数据，未进行翻页*
- *wap: ftag* *无法加载更多数据，未进行翻页*
- *wap: fsubject* *无法加载更多数据，未进行翻页*
- *wap: fevent* *无法加载更多数据，未进行翻页*
- *wap: fplan*无法加载更多数据，未进行翻页*
- *package.json* *index-watch错误以及git地址更正*



# V0.5

## V0.5.x

### Aurora - 0.5.6 (20181014)

- api
    - login
        - (优化) auth 在nigix请求超时时的处理，使用 “set_time_limit(0)”
- index
    - index： 地址图片不在写死，采用模块及常量实现  #180604
    - (修复) index 中 *gitee* 地址错误
- wap
    - (优化) msg/success(erro) 设置默认倒数秒数放回上一页面
- 系统
    - config
        - 规定模块级常量配置命名方案 *p_模块* #180604
    - app/common
        - (修复) *urlBuild* 函数在 nginx 服务器中 url生成出错的问题 #180622
            - 注释内部 *$baseUrl* 参数，直接写为空字符串；需要知道该参数的使用目的 @todo know            
- 依赖
    - bootstrap 4.1.0 ->  4.1.1    #180604
    - ThinkPHP 5.1.15 -> 5.1.17    #180622
- 提交说明
    - 180622: 解决 nigix环境下的错误，以及将php版本设置为7以上

### Aurora - 0.5.5 (20180602)
- assets
    - webpack 更新 v3 -> v4
        - 更新 *package.json* 中的运行脚本，以及重新按照程序所需要的环境
        - 更新 webpack.config.js 脚本使之webpack4 相对应
- 系统
    - (修复) app/common.php 修复 *urlBuild* php版本或者nginx等版本解析错处
    - 系统验证码前端地址使之构造成 **图片地址格式** 
    - thinkphp 升级 v5.1.12 -> v5.1.15

### Aurora - 0.5.4 (20180506)
- Wap
    - faccount
        - detail/财务明细
            - (修复) thinkphp 框架更新以后的db错误 #180502
    - faffair
        - edit
            - (+) 页面引入 **模式** ， 新增时可填写密码，编辑时密码获取为系统设置的掩码 #180502
            - (+) 添加 “口令” 更改界面，且修改时需要登录密码验证。实现的表单设置 #180502
            - (+) 页面之间引入 “描点”保持，即页面刷新以后有效   #180506
            - (+) 加入 **验证码** ，完成表单控制
- Api
    - login/quit
        - (修复) Redis 服务器开启失败的系统错误，采用 try-catch #180502
    - affair/command
        - (+) 新增事务甲乙 **处理口令** 接口，实现口令查看以及修改(需要session验证)
- 系统
    - app\common\Aurora
        - :: userLoginHandler (修复) Redis 服务器错误时引起的系统错误，导致登录出错 #180502
    - (+) app/common/SvFaffair
        - :: getCmd 实现方法，用于获取登录口令     
        - :: getRaw 用于获取原始口令 
    - config/setting
        - (+) 添加 **pwsd_mask/密码掩码**   参数 #180502
    - model::Fnc1000c
        - (修复) 分组统计时，用户条件无效 #180506
    - model::User
        - (优化) getPassword 方法，可接收参数，且 uid 不受当前实例影响
    - 其他
        - 系统中涉及 **验证码** 的表单，禁止表单可自当完成
- Lib
    - thinkphp
        - 5.1.11 -> 5.1.12 #180426
    - crypto-js
        - (+) 3.1.9-1 未使用
    - phpseclib/phpseclib
        - (+) 2.0 

### Aurora - 0.5.3 (20180422)

> 实现与 ***center*** 平台的交互，即用户调整；引入 ***pigeons*** 接口管理，使其用户登录时获取来自 pigeons 平台的 token

- wap
    - faccount 财务账单
        - edit 编辑页面： ***金额*** 不限制数字类型，实现更改后支持等式计算 #180408    
- index
    - login
        - 修复登录成功以后调整无效 #180421    
- Api
    - visit
        - DistributionChart 框架更新后的写法 #180421
    - login
        - (+) token 实现 ***token*** 登录系统，用于其他平台的交互
    - (+) bin 新增接口控制器
        - center 实现，***个人中心*** 的跳转
    - faccount
        - search 修复卡框架更新引起的错误
- 系统
    - app\common\controller\Api        
        - ***调整*** 框架升级造成的错误 #180408
- Lib    
    - thinkPHP
        - framework 升级 #5.1.5 -> 5.1.6 #180330
        - framework 升级 #5.1.6 -> 5.1.8 #180408
        - framework 升级 #5.1.8 -> 5.1.11 #180421
    - git/hyang 从 GitHub 转移到 gitee #180330
    - bootstrap
        - 升级 #4.0.0->#4.1.0

### Aurora - 0.5.2 (20180324)
- TODO  改版本计划处理的和新增的功能(计划)
    - Wap
        - 财务登账“事务甲乙”等控件选取无效     @FB180128-01 (Fixed Bug)
    - 系统
        - 登录以后用户与“aurora_center”项目的交互 @FB180128-02
- API
    - app/common/controller/Api.php
        - FeekMsg: 修复方法 参数为 obejct 时json返回格式更变  @180129
            - 由框架更新后，***数据库模型*** 多数据查询方法对象造成
    - app/api/controller/Faccount.php
        - (新增) faccount/detailSync 明细账单同步接口 @180222
    - app/api/controller/Finance.php
        - (优化) finance/master_get 被禁止的事务甲乙可被提取，用于修改历史财务账单 @180303
        - (新增) finance/status 财务状态查询 #180324
- Wap 
    - *** FB180128-01 *** bug 修复 @180129
    - wap/faccount 财务账单
        - 编辑
            - 编辑页面城市，从上次记录中填写的获取；暂时屏蔽元session ip 分析来的地址，由于地址定位不够准确 @180222
            - 页面标题固定在顶部 #180324
        - 明细
            - 添加 *** 明细账单计算并同步总账单 *** 功能 @180222
    - wap/faffair 事务甲乙
        - 主页-index 从颜色上标记 “use_mk” 为 N 是事务甲乙 @180303
        - 编辑-edit
            - 修复“use_mk” 开关表单控件仅仅单次编辑有效，表单元素类型有问题 @180303    
            - 页面标题固定在顶部 #180324
    - wap/finance 财务系统首页
        - 主页-index 添加 index.js 文件，使用实现对财务状态的查询 #180324
- assets
    - src/Aruora.js 新增 ***numberUnited*** 方法 #180324
- Index
    - index/data
        - index: 处理从后台获取的数据，即进行数据逻辑(去重)判断 #180324
            - 后去除掉，由于逻辑的缺陷；使用数据库对数据源头进行清洗
- 数据库
    - 新增 ***get__curfnc_status*** 存储过程，用于查询用户的财务状态 #180324    
    - ***visit_rank*** 数据源分析数据 #180324
    - ***finance_creport_sp*** 优化sql，在 sum 前利用 ifnull 函数处理 #180324
- 依赖升级
    - boostrap @v4.0.0-beta.3 -> v4.0.0     @180222
    - echarts @v3.8.5           -> v4.0.3   #180324

### Aurora - 0.5.1 (20180128)
- 系统升级适应性
    - ***Index***
        - /index 首页
            - 引入index.css 样式，添加页面背景底色，尽量与前一版本不同     #180111
            - 界面链接更新        #180111
        - /mdreader markdown 文件解析
            - 库 Markdown.php 直接使用系统函数引入     #180111
        - /login 登录页面
            - 验证码错处修复； 由js引入错误，地址加了项目前缀     #180111
    - ***Wap***
        - 头文件中 ***icon*** 图标地址更新    #180111      
    - 系统模块更新 @180128
        - like 字段查询： [col] => [like, value]  更新为 [col, like, value]
    - app/common.php    @180128
        - debugOut 函数调试模式无效修复
- ***Wap***
    - 首页样式，头部控件添加与Web一样的底色(与0.4.x区分开)   #180112
    - 财务登账 @180128
        - 财务搜索
            - 新增按键快捷键"Enter" 搜索内容     
            - 搜索时不区分大小写
- ***admin***
    - 系统菜单名称更改为 ***v0.5.admin_tpl*** @180120
    - app/common/traits/Admin::getParamFromMenu 数据筛选条件使用“控制器”，而替换掉“地址”匹配 @180120
- assets
    - assets/src/web/FormListener.js 修复 ***formAction*** 内部 this 指向错误 @180120

### Aurora - 0.5.0 (20180110)
- 安装 tp5.1 框架相关依赖
    - composer create-project topthink/think suroong    #180102
    - composer require topthink/think-captcha           #180102
- 项目更新升级
    - 根据 ***tp5.1.x*** 的特性升级原系统的相关目录配置
        - 新的项目将采用独立端口号指向， 于 ***Aurora-0.x*** 不同 #180102
    - 系统配置文件、更名名称等更新    #180102
    - ***Aurora/Index*** 项目调整以及测试
        - 调整视图文件、未使用 ***app\common\traits\WebIndex*** 类的页面正常 #180102
    - webpack 
        - assets/webpack.config.js 新的目录更新 #180104
        - assets/src/Config.js baseurl 地址更新 #180104
        - 编译以后的js不再上传到 git 仓库， 由于打包的以后的更变文件太多而不便于日志编写 #180104
    - ***Index*** 模块
        - app/common/traits/WebIndex.php 原来传入的 view 对象更新，使用 hyang/Dataset(新增类到hyang 创库) 类替换 #180104
            - 本地运行基本正常     #180104
    - @180110
        - Index/Admin/Wap 项目地址路径适应更变
            - 视图/图片指向地址的改变
            - 框架更新后 ***命名空间*** 调整
            - 直接使用 ***PHPStrom*** 的路径替换功能
        - 经过测试基本修复所发现的问题
- 项目说明
    - 版本号更变 ***1.5.x*** -> ***0.5.x*** 更新 php 后台框架依然改变大版本号 #181004        
    - 将 webpack 编译的文件上传到git仓库，由于包含图片以及css(手动添加)
