<?php
/**
 * Auther: Joshua Conero
 * Date: 2017年7月20日 星期四
 * Email: brximl@163.com
 * Name: redis 相关设置
 */

 return [
     'ref'     => '__aurora',                // redis 设置前缀
     'default' => [
         'engin'    => 'phpredis',           // php_redis.dll(phpredis)
         'host'     => '地址',
         'port'     => 6379
     ]
 ];