<?php
/**
 * Created by PhpStorm.
 * User: Joshua Conero
 * Date: 2019/7/19
 * Time: 15:57
 * Email: conero@163.com
 */

// +----------------------------------------------------------------------
// | 邮件设置
// +----------------------------------------------------------------------

return [
    'minute'    => 15,
    'default'   => '<a href="https://aurora.conero.cn/">【CONERO.CN】</a>  您好{name}，验证码【<b>{code}</b>】，请在{minute}分钟内按页面提交验证码，切勿将验证码泄露于他人。祝您有愉快的一天！！',
    'default_subject' => '来自【CONERO.CN】验证码消息'
];