/**
 * webpack 打包处理事件
 * 2017年12月31日 星期日
 */
const fs = require('fs')

const Pkg = require('../package.json')
// 用于测试测试订中断程序
var die = (msg) => {
    throw new Error(msg)
}

class BuildBeforeHandler{
    constructor(){
        this.createVersionInfo()
    }
    /**
     * 生成json 版本信息
     */
    createVersionInfo(){
        var json = {
            version: Pkg.version,
            release: Pkg.release            
        }
        fs.writeFileSync('./version', JSON.stringify(json, null, 4))
    }
}
class Event{
    static onBuildBefore(){
        new BuildBeforeHandler()
    }
}

module.exports = {
    Event
}