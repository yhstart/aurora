/**
 * admin 项目打包配置
 * 2017年12月27日 星期三
 */

const {
    AuroraJsPacker,
    nodeEnv
}  = require('./webpack.config')



var options = {
    baseDir: 'admin/js/'
}
var FileQueue = (new AuroraJsPacker(options)).js([
    'logger/index.js',
    'logger/msg.js',
    'logger/sfile.js',
    'menu/edit.js',
    'project/about.js',
    'project/edit.js',
    'project/news.js',
    'sconst/index.js',
    'sconst/edit.js',
    'sgroup/index.js',
    'sgroup/edit.js',
    'skey/index.js',
    'token/index.js',
])
.getFiles()

//console.log(FileQueue)

module.exports = FileQueue