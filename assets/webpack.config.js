/**
 * webpack 公共处理包程序
 */

const webpack = require('webpack')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin')           // 文档压缩插件
const $event = require('./webpack.event')
const nodeEnv = process.env.NODE_ENV                                // 运行环境
const path = require('path')

var baseDir;
class AuroraJsPacker{
    constructor(option){
        // 打包的文件集合
        this.PackerFiles = []
        this.entry = null   // 输入参数值
        option = 'object' == typeof option? option: {}
        baseDir = ''
        if(option.baseDir){
            baseDir = option.baseDir
        }
        // 编辑处理
        $event.Event.onBuildBefore()        
    }
    /**
     * @param {string|string[]} src 
     */
    js(src){
        src = 'object' == typeof src? src : [src]
        if(!this.entry){
            this.entry = {}
        }
        let _baseDir = './assets/' + baseDir
        // 遍历处理
        src.forEach((v, i) => {
            // console.log(v, i)
            const extFile = v.substr(-3).toLowerCase();
            let tag = v;
            if( extFile != '.js' && extFile != '.ts'){
                v += '.js'
                tag = v;
            }else if(extFile === '.ts'){
                tag = v.replace('.ts', '.js')
            }
            // src[i] = `${_baseDir}${v}`
            this.entry[tag] = `${_baseDir}${v}`
        })
        // this.entry = [].concat(this.entry, src)
        return this
    }
    getFiles(){
        let {entry} = this;
        let config = {
            // mode: "development", // "production" | "development" | "none"
            entry,
            output: {
                // path: path.resolve(__dirname, sR.path), // string
                path: path.resolve(__dirname, '../public/static/'),
                // path: './public/static/',
                // filename: "[name].js", // for multiple entry pointscls
                filename: `${baseDir}[name]`
            },
            module: {
                rules: [
                    {
                        test: /\.js?$/,
                        loader: "babel-loader",
                        // the loader which should be applied, it'll be resolved relative to the context
                        // -loader suffix is no longer optional in webpack2 for clarity reasons
                        // see webpack 1 upgrade guide
                        options: {
                            presets: ["es2015"]
                        }
                    },{
                     test: /\.tsx?$/,
                     loader: "ts-loader"
                    }
                ]
                // loaders: [
                //     {
                //         //test: path.join(__dirname, 'es6'),
                //         loader: 'babel-loader',
                //         query: {
                //             presets: ['es2015']
                //         }
                //     }
                // ]
            },
            resolve: {
                extensions: ['.js', '.json', '.ts']
            },
            // devtool: "source-map", // enum
            target: "web", // enum
            // fail out on the first error instead of tolerating it.
            cache: true // boolean    
        }
        return config
    }
}
module.exports = {
    AuroraJsPacker,
    nodeEnv
}