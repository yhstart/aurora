// 2019年8月13日 星期二
// 移动端【用户认证】

// declare var $: JQuery|Function;
declare var $: any;
declare var weui: any;
declare var Wap: instance;

import {
    instance
} from '../../../src/Wap'
// 验证库
import Validate from '../../../src/Validate'

// 系统全局性变量
(<any>window).Wap = instance;



(<any>$)(function () {
    class App{
        constructor(){
            this.domListener()
        }
        // dom 事件驱动
        domListener(){
            // 获取验证码
            $('#get_verify_code').click(function () {
                let $this = $(this);
                if($this.data('disabled') == '1'){
                    return;
                }
                let email = $('#email_contrl').val();
                if(email){
                    if(Validate.isEmail(email)){
                        //
                        let sec: number = 120,
                            iSec: number = sec,
                            timerIns: number
                        ;

                        timerIns = setInterval(function () {
                            if(iSec <= 0){
                                clearInterval(timerIns);
                                $this.attr('class', 'weui-vcode-btn');
                                $this.data('disabled', '0');
                                $this.text('获取验证码');
                                return;
                            }
                            iSec -= 1;
                            $this.text(`请${iSec}s后重试`);
                            $this.attr('class', 'weui-btn weui-btn_disabled');
                            $this.data('disabled', '1');
                        }, 1000);

                        // 发送邮件
                        Wap.ApiRequest('verify/code',{email},function (rs) {
                            if(rs.code == 1){
                                weui.toast(rs.msg);
                            }else{
                                weui.topTips(rs.msg);
                            }
                        });
                    }else{
                        $('#email_contrl').focus();
                        weui.topTips('【邮箱】格式不对！');
                    }
                }else {
                    $('#email_contrl').focus();
                    weui.topTips('【邮箱】为空！');
                }
            });
            // 提交
            $('#save_lnk').click(function () {
                weui.form.validate('.js__form', function (error) {
                    if (!error) {
                        var savedata = Wap.formJson('.js__form');
                        // 发送邮件
                        Wap.ApiRequest('verify/check',savedata,function (rs) {
                            if(rs.code == 1){
                                weui.toast(rs.msg);
                                setTimeout(function () {
                                    location.href = '/wap.html';
                                }, 2500);
                            }else{
                                weui.topTips(rs.msg);
                            }
                        });
                    }
                });
            });
        }
    }

    new App()
});