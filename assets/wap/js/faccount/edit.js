/**
 * Created by Administrator on 2017/6/13 0013.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    var cacheData = {
        tag: null               // 标签值
    };
    // 右滑动时返回
    Wap.SwipeRightBack();
    
    var MastOpts = null,
        TagIdOpts = null,
        SlaveOpts = null,
        SplanOpts = null
        ;
    /**
     * 获取master 可选参数
     */
    function getMasterPickerOption() {
        if(!MastOpts){
            Wap.ApiRequest('finance/master_get',null,function (rdata) {
               MastOpts = rdata.data;
            });
        }
    }
    // 金额监听
    $('#money_ipter').change(function(){        
        let dom = $(this)
        let money = dom.val()        
        let reg = /[0-9\*-\+\/\.]/
        if(money && reg.test(money)){
            try {
                // 检测是否合法js等式
                money = eval(money)
                dom.val(money)
            } catch (error) {
            }
        }
        if(money && isNaN(money)){
            dom.val('')  
            weui.topTips(`【${money}】 不是有效数字/等式`)
        }        
    })
    // 事务设置
    $('#master_setter_btn').click(function () {
        if(!MastOpts) getMasterPickerOption();
        if(!MastOpts) return;
        weui.picker(MastOpts,{
            className: 'custom-classname',
            onConfirm: function (result) {
                $('#master_ipter').val(result[0].label);
                $('#masterid_ipter').val(result[0].value);
            },
            id: 'singleLinePicker'
        });
    });

    /*// 标签 输入框控制
    $('#tagid_desc_ipter').change(function () {
        var dom = $(this);
        var tagText = dom.val();
        $('#tagid_ipter').val('');
        if(tagText){
            Wap.ApiRequest('finance/tag_check_get',{tag:tagText},function (rdata) {
                if(rdata.code == 1){
                    var data = rdata.data;
                    if(data.length == 1) {
                        data = data[0];
                        $('#tagid_desc_ipter').val(data.label);
                        $('#tagid_ipter').val(data.value);
                    }else if(data.length > 1){
                        weui.picker(data,{
                            className: 'custom-classname',
                            onConfirm: function (result) {
                                $('#tagid_desc_ipter').val(result[0].label);
                                $('#tagid_ipter').val(result[0].value);
                            },
                            id: 'tagidLinePicker'
                        });
                    }
                    else{
                        $('#tagid_desc_ipter').val('');
                    }
                }
                else{
                    weui.alert(rdata.msg);
                    $('#tagid_desc_ipter').val('');
                }
            });
        }else{
            $('#tagid_desc_ipter').val('');
        }
    });*/
    // 标签 协助输入框
    $('#tagid_setter_brn').click(function () {
        function tagIdPicker() {
            weui.picker(TagIdOpts,{
                className: 'custom-classname',
                onConfirm: function (result) {
                    $('#tagid_desc_ipter').val(result[0].label);
                    $('#tagid_ipter').val(result[0].value);
                },
                id: 'tagidLinePicker'
            });
        }
        if(null !== TagIdOpts && TagIdOpts) tagIdPicker();
        else{
            Wap.ApiRequest('finance/tagid_get_picker',null,function (rdata) {
                TagIdOpts = rdata.data;
                tagIdPicker();
            });
        }
    });

    // 事务乙方下拉框设置
    $('#slave_setter_btn').click(function () {
        function slavePicker() {
            weui.picker(SlaveOpts,{
                className: 'custom-classname',
                onConfirm: function (result) {
                    $('#slave_ipter').val(result[0].label);
                    $('#slaveid_ipter').val(result[0].value);
                },
                id: 'slaveidLinePicker'
            });
        }
        if(SlaveOpts){
            slavePicker();
        }
        else{
            Wap.ApiRequest('finance/slave_get_picker',null,function (rdata) {
                SlaveOpts = rdata.data;
                slavePicker();
            });
        }
    });
    // 事务乙方 自动生成
    $('#slave_ipter').change(function () {
        var salve = $(this).val();
        $('#slaveid_ipter').val('');
        if(salve){
            Wap.ApiRequest('finance/slave_check_picker',{salve:salve},function (rdata) {
                if(rdata.code == 1){
                    weui.picker(rdata.data,{
                        className: 'custom-classname',
                        onConfirm: function (result) {
                            $('#slave_ipter').val(result[0].label);
                            $('#slaveid_ipter').val(result[0].value);
                        },
                        id: 'slaveidLinePicker'
                    });
                }
            });
        }
        else{
            $('#slave_ipter').val('');
        }
    });
    // 财务计划绑定
    $('#splan_setter_btn').click(function () {
        function splanPicker() {
            weui.picker(SplanOpts,{
                className: 'custom-classname',
                onConfirm: function (result) {
                    $('#splanno_desc_ipter').val(result[0].label);
                    $('#splan_no_ipter').val(result[0].value);
                },
                id: 'splanLinePicker'
            });
        }
        Wap.ApiRequest('finance/splan_get_picker',null,function (rdata) {
            if(rdata.code == 1){
                SplanOpts = rdata.data;
                splanPicker();
            }else{
                weui.alert('无法获取【财务计划】数据!');
            }
        });
        if(SplanOpts) splanPicker();
    });

    // 标签实现
    let tagid = instance.tags('#tagid_desc_ipter', {
        url: 'finance/get_tags',
        valueKey: 'search',
        nameKey: 'tag',
        idKey: 'listid'
    });
    tagid.create(instance.getJsVar('tag_list'));

    // 数据保存
    weui.form.checkIfBlur('.js__form');
    $('#submit_lnk').click(function () {
        var loading = instance.weuiLoading('数据提交中……');
        weui.form.validate('.js__form', function (error) {
            if(!error){
                let savedata = Wap.formJson('.js__form');
                // 标签
                savedata.tags = tagid.data;
                let savedataFunc;
                savedataFunc = function(postData){
                    Wap.ApiRequest('faccount/save',postData,function (data) {
                        loading.hide();
                        if(data.code == -1){
                            let dd = data.data;
                            if(dd && dd.add_check && dd.add_check == 1){
                                weui.confirm(data.msg, function () {
                                    // 重新提交
                                    postData.add_check = 1;
                                    savedataFunc(postData);
                                });
                                return null;
                            }
                            weui.alert(data.msg);
                            return null;
                        }
                        Wap.msg_success(data.msg,'财务系统.记账');
                    });
                };
                savedataFunc(savedata);
            }else{
                loading.hide();
            }
        });
    });
    getMasterPickerOption();

    // 自动完成
    instance.autocomplete('#name_ipter', 'fnc1000c.name');
});