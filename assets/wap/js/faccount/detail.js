/**
 * Created by Administrator on 2017/7/2 0002.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    var detailNo = null; // 明细账单号， Y(禁止保存),no 可修改，
    // 右滑动时返回
    Wap.SwipeRightBack('faccount.html');
    function exchangeBar(target) {
        var dom = $('div.aurora-hidden');
        var id = dom.attr('id');
        //var queue = ['list','edit'];
        //dom.removeClass('aurora-hidden');
        $('div.weui-tab__panel').addClass('aurora-hidden');
        target = target? target:'#'+id;
        $(target).removeClass('aurora-hidden');
        location.href = target;
    }
    // 新增明细链接
    $('#add_detail_lnk').click(function () {
        detailNo = null;
        // 移除删除栏
        var lnk = $('#row_del_lnk');
        if(lnk.length > 0) lnk.remove();
        Wap.ResetForm('#edit',['part']);
        exchangeBar();
    });
    // 删除字段
    function delBtn() {
        var form = $('#edit').find('.weui-cells_form');
        var lnk = form.find('#row_del_lnk');
        if(lnk.length == 0 && (detailNo && 'Y' != detailNo)){
            var xhtml = '<a class="weui-cell weui-cell_access" href="javascript:;" id="row_del_lnk">'
                + '<div class="weui-cell__bd text-warning">'
                + '<p>删除该明细</p>'
                + '</div>'
                + '<div class="weui-cell__ft"></div>'
                + '</a>';
            form.append(xhtml);
            $('#row_del_lnk').off('click').on('click',function () {
               weui.confirm('您确定要删除该账单明细吗？',function () {
                    Wap.ApiRequest('faccount/detail',{mode:'D','no':Base64.encode(detailNo)},function (rdata) {
                        if(rdata.code == '1') $('a[data-no="'+detailNo+'"]').remove();
                        weui.topTips(rdata.msg);
                        exchangeBar();
                    });
               });
            });
        }
    }
    // 账单修改调整
    $('#list').find('a[data-no]').click(function () {
       var id = $(this).data('no');
        Wap.ApiRequest('faccount/get_detail',{no:id},function (rdata) {
           if(rdata.code == 1){
               exchangeBar('#edit');
               var data = rdata.data;
               for(let key in data){
                   let ipt = $(`#${key}_ipter`);
                   if(ipt.length > 0){
                       ipt.val(data[key]);
                   }
               }
               detailNo = id;
               delBtn();
           }
            else{
               weui.alert(rdata.msg);
           }
        });
    });
    // 表单提交
    var srcNo = Wap.getUrlBind('item');
    var ListSum = parseFloat($('#sum_col').text());
    weui.form.checkIfBlur('#edit');
    // 保存按钮
    $('#edit_submit_lnk').click(function () {
        if(Wap.empty(srcNo)){
            weui.topTips('请求参数获取失败，无法保存数据！');
            return;
        }
        if('Y' === detailNo){
            weui.topTips('该数据禁止保存！');
            return;
        }
        var loading = weui.loading('数据提交中……');
        weui.form.validate('#edit', function (error) {
            if(!error){
                var savedata = Wap.formJson('#edit');
                if(detailNo){savedata.no = Base64.encode(detailNo);}
                else savedata.src_no = srcNo;
                Wap.ApiRequest('faccount/detail',savedata,function (data) {
                    loading.hide();
                    if(data.code == -1){
                        weui.alert(data.msg);
                        return null;
                    }
                    if(detailNo){
                        var aEl = $('a[data-no="'+detailNo+'"]');
                        if(aEl.length > 0){
                            aEl.find('div.weui-cell__bd > p').text(savedata.name);
                            aEl.find('.weui-cell__ft').html('<span style="color:#FF6633;">' + (parseFloat(savedata.money) * parseInt(savedata.part)) +
                                '</span> (' + savedata.money + ' * ' + savedata.part + ')');
                        }
                    }
                    else {
                        var listXhtml = '<a class="weui-cell weui-cell_access" href="javascript:;">' +
                                '<div class="weui-cell__bd">' +
                                '<p>' + savedata.name + '</p>' +
                                '</div>' +
                                '<div class="weui-cell__ft"><span style="color:#FF6633;">' +
                                (parseFloat(savedata.money) * parseInt(savedata.part)) +
                                '</span> (' + savedata.money + ' * ' + savedata.part + ')' +
                                '</div>' +
                                '</a>'
                            ;
                        $('#list').find('.weui-cells').append(listXhtml);
                    }
                    ListSum = ListSum + (parseFloat(savedata.money) * parseFloat(savedata.part));
                    $('#sum_col').text(ListSum);
                    //exchangeBar('#list');
                    Wap.ResetForm('#edit',['part', 'unit']);
                    weui.toast(data.msg);

                    //修改时跳转
                    if(savedata.no){
                        exchangeBar('#list');
                    }

                });
            }else loading.hide();
        });
    });
    // 数据同步
    $('#sync-set').click(function(){
        var loading = instance.weuiLoading('数据加载中')
        var postdata = {no: srcNo}
        Wap.ApiRequest('faccount/detailSync', postdata, function (data) {
            loading.hide()
            if(1 == data.code){
                location.reload(true)
            }
        })
    });

    // 事务驱动
    instance.autocomplete('#name_ipter', 'fnc1001c.name');
    instance.autocomplete('#unit_ipter', 'fnc1001c.unit');

    // 自动适应
    exchangeBar(location.hash? location.hash: '#list');

});