/**
 * Created by Administrator on 2017/6/21 0021.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    var page = 1;
    var maxPage;
    // 滑动特效
    function panelSwipe(dom) {
        var id = dom.attr('id');
        dom.toggleClass('aurora-hidden');
        var targetId = '#' + (id == 'home'? 'edit':'home');
        $(targetId).toggleClass('aurora-hidden');
    }
    // 加载数据
    function loadData(){
        Wap.ApiRequest('fsubject/sets',{page},function (rdata) {
            if(rdata.code == 1){
                //console.log(rdata);
                var dd = rdata.data;
                maxPage = Wap.getAllPage(dd.count, dd.num);
                // console.log(dd, maxPage);
                var rs = dd.result;
                var xhtml = '';
                for(var k in rs){
                    var el = rs[k];
                    var endMk = el.end_mk;
                    var pMk = el.private_mk;
                    var icon = pMk == 'Y'? '<i class="fa fa-user-o text-info"></i>':'<i class="fa fa-globe text-success"></i>';
                    xhtml += 
                        '<a class="weui-cell'+(pMk == 'Y'? ' weui-cell_access':'')+'" href="javascript:;">' +
                            '<div class="weui-cell__bd">' +
                                '<p>'+ icon+' '+ el.subject +'</p>' +
                            '</div>' + 
                            '<div class="weui-cell__ft" style="font-size: 0.78em;">' + (pMk == 'Y'? '':el.author+ '/')+ el.date + '</div>' +
                        '</a>'
                    ;                   
                }
                if(xhtml){
                    var $html = $(xhtml);
                    $('#home > .weui-cells').append($html);
                }else{
                    html = 
                    `
                    <a class="weui-cell weui-cell_access" href="`+Wap.getWapUrl('fplan/edit', true)+`">
                        <div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>
                        <div class="weui-cell__ft">
                            您还没一条财务记录
                        </div>
                    </a>
                    `;
                    var $html = $(xhtml);
                    $('#home > .weui-cells').html($html);
                }                    
            }
            else{
                weui.alert(rdata.msg);
            }
        });
    }
    // 右滑动时返回
    Wap.SwipeRightBack();

    var $panel = $('.weui-tab__panel');
    // tag 滑动切换 -> 查看与新增
    $panel.swipeLeft(function () {
        panelSwipe($(this));
    });
    
    /**
     * 自动适应描点
     * @param hash
     */
    function AdjustRequest(hash) {
        hash = hash? hash : location.hash;
        if(hash){
            var curTab = $(hash);
            if(curTab.length > 0 && curTab.attr('class').indexOf('aurora-hidden') > -1){
                $('.weui-tab__panel').addClass('aurora-hidden');
                curTab.removeClass('aurora-hidden');
            }
            location.hash = hash;
        }
    }
    AdjustRequest();
    // tab 页面跳转
    $('.js__goto').click(function () {
        var id = $(this).attr('href');
        if(id) AdjustRequest(id);
        //console.log(id);
    });
    // 编辑页面保存
    $('#edit_submit_lnk').click(function () {
        var subject = $('#subject_ipter').val();
        if(subject == ""){
            weui.topTips('请设置科目!',2000);
            return;
        }
        var loading = instance.weuiLoading('数据提交中...');
        var privateMk = $('#prvtmk_ipter').is(':checked')? 'N':'Y';
        var saveData = {subject:subject,private_mk:privateMk};
        Wap.ApiRequest('finance/subject_save',saveData,function (rdata) {
            loading.hide();
            if(rdata.code == -1){
                weui.alert(rdata.msg);
                return;
            }
            weui.toast(rdata.msg);
            // 表单重置
            $('#subject_ipter').val('');
            $('#prvtmk_ipter').attr('checked',true);
            AdjustRequest('#home');
        });
    });    
    // 运行台
    loadData();

    // 加载到底部时自动翻页
    $('a.weui-footer__link').click(function () {
        if(page < maxPage){
            page += 1;
            loadData();
        }else{
            weui.topTips('没有更多数据了，朋友！');
        }
    });
});