/**
 * Created by Administrator on 2017/6/24 0024.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    // 右滑动时返回
    Wap.SwipeRightBack('fevent.html');
    // 表单提交
    weui.form.checkIfBlur('.js__form');
    $('#submit_lnk').click(function () {
        weui.form.validate('.js__form', function (error) {
            if(!error){
                var loading = instance.weuiLoading('数据提交中……');
                Wap.ApiRequest('fevent/save',Wap.formJson('.js__form'),function (data) {
                    loading.hide();
                    if(data.code == -1){
                        weui.topTips(data.msg);
                        return null;
                    }
                    Wap.msg_success(data.msg,'财务纪事');
                });
            }
        });
    });
});