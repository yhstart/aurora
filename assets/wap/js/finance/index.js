/**
 * 2018年3月24日 星期六
 * 财务界面首页
 */

import {
    instance
} from '../../../src/Wap'

// 系统全局性变量
window.Wap = instance

$(function(){
    class App{
        constructor(){
            this.Loader()
        }
        // 数据加载
        Loader(){
            instance.ApiRequest('finance/status', null, function(rs){
                var data = rs.data
                if(data.num){
                    $('#faccount_count').html(
                        '<span title="当前财务盈余">' + instance.numberUnited(data.surplus) + '￥</span>/' + 
                        instance.numberUnited(data.num) + '条'
                    )
                    $('.page__hd').append(
                        `
                        <div class="weui-flex">
                            <div class="weui-flex__item">
                                <div class="placeholder">
                                    年度：`+ (data.annual_num > 0? `
                                    ${instance.numberUnited(data.annual_sps)}/${instance.numberUnited(data.annual_num)}
                                    `: '0条') +`
                                </div>
                            </div>      
                            <div class="weui-flex__item">
                                <div class="placeholder">
                                    月度：`+ (data.month_num > 0? `
                                    ${instance.numberUnited(data.month_sps)}/${instance.numberUnited(data.month_num)}
                                    `: '0条') +`
                                </div>
                            </div>
                        </div>
                        <div class="weui-flex">
                            <div class="weui-flex__item">
                                <div class="placeholder">
                                    今日：`+ (data.daily_num > 0? `
                                    ${instance.numberUnited(data.daily_sps)}/${instance.numberUnited(data.daily_num)}
                                    `: '0条') +`
                                </div>
                            </div>      
                            <div class="weui-flex__item">
                                <div class="placeholder">
                                    前日：`+ (data.lastd_num > 0? `
                                    ${instance.numberUnited(data.lastd_sps)}/${instance.numberUnited(data.lastd_num)}
                                    `: '0条') +`
                                </div>
                            </div>
                        </div>
                        `
                    );
                }
            })
        }
    }
    new App()
})