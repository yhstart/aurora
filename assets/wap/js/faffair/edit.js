/**
 * Created by Administrator on 2017/6/13 0013.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    // 右滑动时返回
    Wap.SwipeRightBack('faffair.html');
    class App{
        constructor(){
            this.listnerDom()
        }
        /**
         * dom 监听
         */
        listnerDom(){
            var $this = this;
            // 数据保存
            weui.form.checkIfBlur('.js__form');
            $('#submit_lnk').click(function () {
                weui.form.validate('.js__form', function (error) {
                    if(!error){
                        Wap.ApiRequest('faffair/save',Wap.formJson('.js__form'),function (data) {
                            if(data.code == -1){
                                weui.alert(data.msg);
                                return null;
                            }
                            Wap.msg_success(data.msg,'事务甲乙');
                        });
                    }
                });
            });
            // 分组可选
            $('#groupMk_sel').click(function () {
                var group = Wap.getJsVar('groups');
                // console.log(group);
                var pickerOption = [];
                for(var i=0; i<group.length; i++){
                    if(group[i]){
                        pickerOption.push({
                            label:group[i]
                        });
                    }
                }
                weui.picker(pickerOption, {
                    onConfirm: function (result) {
                        $('#groupmk_ipter').val(result[0].label);
                    }
                });
            });
            // 修改密码
            $('#js__cmd_lnk').click(function(){
                $this.page('cmd')
            })
            // 锚点恢复
            var hash = location.hash
            if(hash){
                this.page(hash.substr(1))
            }            
        }
        /**
         * 设置 cmd 页面
         */
        createCmdPg(){
            var $this = this
            var name = 'cmd',
                cls = 'js__' + name
            
            var container = $('.container')
            var $page = container.find('.' + cls)
            if($page.length == 0){
                container.append('<div class="aro-page '+cls+'"></div>')
                $page = container.find('.' + cls)
            } 
            var pageHtml = `
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label for="command_ipter" class="weui-label">登录确定密码</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="password" name="pwd" value="" id="pswd_ipter" required emptyTips="请输入用户登录密码以验证身份">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label for="command_ipter" class="weui-label">保密口令</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="text" name="command" value="" id="command_ipter">
                    </div>
                </div>
                <div class="weui-flex aurora-border"></div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                    </div>
                    <div class="weui-cell__ft">
                        <a href="javascript:;" id="recode_lnk"><img class="weui-vcode-img" src="/captcha.html?t=${Math.random()}" /></a>
                    </div>
                </div>
                <div class="weui-cell weui-cell_vcode">
                    <div class="weui-cell__hd"><label class="weui-label">验证码</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" autocomplete="off" name="code" type="text" required emptyTips="请输入密码" placeholder="请输入验证码"/>
                    </div>
                </div>
                <div class="weui-flex aurora-border"></div>
                <div class="weui-btn-area">
                    <a class="weui-btn weui-btn_primary js__submit" data-type="change" href="javascript:">修改密码</a>
                </div>
                <div class="weui-btn-area">
                    <a class="weui-btn weui-btn_warn js__submit" data-type="view" href="javascript:">查看密码</a>
                </div>
                <div class="weui-cells">
                    <a class="weui-cell weui-cell_access" href="javascript:;" id="js__main_lnk">
                        <div class="weui-cell__bd">
                            <p>返回表单</p>
                        </div>
                        <div class="weui-cell__ft">数据编辑</div>
                    </a>
                </div>
            `
            $page.html(pageHtml)
            // 刷新验证码
            var reflashCodeFn = () => {
                $('#recode_lnk').find('img').attr('src', '/captcha.html?t='+Math.random())
            }
            // 返回主页面
            $page.find('#js__main_lnk').click(function(){
                $this.page('main')
            })
            // 验证码刷新
            $page.find('#recode_lnk').click(function(){reflashCodeFn()})
            // 数据提交
            $page.find('.js__submit').click(function(){
                var dom = $(this)
                var type = dom.data('type')
                weui.form.validate('.'+cls, function (error) {
                    if(!error){
                        var saveData = Wap.formJson('.' + cls)
                        $page.find('input[name="code"]').val('')                        
                        if('change' == type){
                            if(!saveData.command || '' == saveData.command){
                                weui.topTips('【保密口令】不可为空')
                                return null
                            }
                        }else if('view' == type){
                            $page.find('input[name="command"]').val('')
                        }
                        saveData.type = type
                        saveData.listid = Wap.getUrlBind('uid')
                        instance.ApiRequest('faffair/command', saveData, function(rs){
                            if(-1 == rs.code){                                
                                reflashCodeFn()
                                weui.topTips(rs.msg)
                                return null                                
                            }
                            if('view' == type){
                                var $input = $page.find('input[name="command"]'),
                                    raw = rs.data.raw || false
                                if(raw){
                                    $input.val(rs.data.raw)
                                }else{
                                    weui.topTips('该账号未设置口令!')
                                }
                            }else{
                                weui.topTips(rs.msg)
                            }
                            reflashCodeFn()
                        })                        
                    }
                })
            })
        }
        /**
         * 内部子页面
         */
        page(name){
           $('.aro-page').hide()
           var hash = name
           name = name? ('.js__' + name) : null
           if(name){
               var dom = $(name)
               if('cmd' == hash && dom.length == 0){
                    this.createCmdPg()
               }
               dom.show()
               location.href = '#'+hash
           }
        }
    }
    new App()
});