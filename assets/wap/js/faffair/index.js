/**
 * Created by Administrator on 2017/6/20 0020.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    var page = 1;   // 分页
    var maxPage;
    class App{
        constructor(){
            this.domEventListener();
            this.loadData();
        }
        // dom 时间绑定
        domEventListener(){
            // 滑动无效
            // // 右滑动时返回或上一页
            // $(document).swipeRight(function () {
            //     // 上一页
            //     if(page != maxPage && page > 1){
            //         location.href = Wap._wapurl + 'faffair.html?page='+(page - 1);
            //         return;
            //     }
            //     if(typeof callback == 'function'){
            //         if(!callback()) return;
            //     }
            //     location.href = Wap._wapurl + 'finance.html';
            // });
            // // 下一页
            // $(document).swipeRight(function () {
            //     if(page < maxPage){
            //         location.href = Wap._wapurl + 'faffair.html?page='+(page + 1);
            //     }else{
            //         weui.topTips('没有更多数据了，朋友！');
            //     }
            // });
            let $this = this;
            // 加载到底部时自动翻页
            $('a.weui-footer__link').click(function () {
                if(page < maxPage){
                    page += 1;
                    $this.loadData();
                }else{
                    weui.topTips('没有更多数据了，朋友！');
                }
            });
        }
        /**
         * 数据获取
         * @param {bool} recovert 是否覆盖对象
         * @memberof App
         */
        loadData(recovert){
            Wap.ApiRequest('faffair/sets',{page},function (rdata) {
                if(rdata.code == 1){
                    //console.log(rdata);
                    var dd = rdata.data;
                    maxPage = Wap.getAllPage(dd.count, dd.num);
                    App.renderData({
                        count: dd.count,
                        page: dd.page,
                        all_page: maxPage
                    });
                    var rs = dd.result;
                    var xhtml = '';
                    for(var k in rs){
                        var el = rs[k];
                        var type = el.type;
                        var icon;
                        if(type == 'M0') icon = '<i class="fa fa-user text-success"></i> ';
                        else if (type == 'S0') icon = '<i class="fa fa-info-circle text-info"></i> ';
                        else icon = '<i class="fa fa-question text-primary"></i> ';
                        xhtml += 
                            '<div class="weui-flex aurora-border"></div>' +
                            '<div class="weui-panel">' +
                                '<div class="weui-panel__hd">' +
                                icon + '<span style="font-size: 1.32em;color:'+(el.use_mk == 'Y'? '#CC6600':'gray')+';margin-left:5px;">'+ el.name + '</span> '+ (el.set_count? '('+ el.set_count +')':'') + 
                                '<span style="float:right;font-style: italic;">'+ el.mtime +'</span>' + 
                                '</div>' +
                                '<div class="weui-panel__bd">' + 
                                    '<div class="weui-media-box weui-media-box_small-appmsg" style="padding-left:50px;">' + 
                                        '<div class="weui-cells">'+ 
                                            '<a class="weui-cell weui-cell_access" style="height:15px;font-size:0.78em;" href="'+ 
                                                Wap.getWapUrl('faffair/edit/uid/'+el.listid, true)+'">' + 
                                                '<div class="weui-cell__hd"><i class="fa fa-pencil-square text-primary"></i></div>' + 
                                                '<div class="weui-cell__bd weui-cell_primary">' +
                                                    '<p>编辑</p>' + 
                                                '</div>' + 
                                                '<span class="weui-cell__ft"></span>' + 
                                            '</a>' + 
                                             (el.set_count? '':
                                             '<a class="weui-cell weui-cell_access row_del_lnk" style="height:15px;font-size:0.78em;" href="javascript:;" data-no="'+ Base64.encode(el.listid+'')+'">' + 
                                                '<div class="weui-cell__hd"><i class="fa fa-trash text-danger"></i></div>' +
                                                '<div class="weui-cell__bd weui-cell_primary">' +
                                                    '<p>删除</p>' +
                                                '</div>' +
                                                '<span class="weui-cell__ft"></span>' +
                                            '</a>') +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        ;
                    }
                    if(xhtml){
                        var $html = $(xhtml);
                        // 事件绑定 - 数据删除
                        $html.find('.row_del_lnk').click(function(){
                            var dom = $(this);
                            var listid = dom.data('no');
                            weui.confirm('您确定要删除数据吗？',function () {
                                var loadding = instance.weuiLoading('数据提交中……');
                                Wap.ApiRequest('faffair/save',{mode:'D',listid:listid},function (rdata) {
                                   loadding.hide();
                                   if(rdata.code == 1) dom.remove();
                                   weui.topTips(rdata.msg);
                               });
                           });
                        });
                        if(recovert) $('#js__data').html($html);
                        else $('#js__data').append($html);
                    }else{
                        html = 
                        `
                        <a class="weui-cell weui-cell_access" href="`+Wap.getWapUrl('faffair/edit', true)+`">
                            <div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>
                            <div class="weui-cell__ft">
                                您还没一条财务记录
                            </div>
                        </a>
                        `;
                        var $html = $(xhtml);
                        $('#js__data').html($html);
                    }                    
                }
                else{
                    weui.alert(rdata.msg);
                }
            });
        }
        /**
         * 数据渲染
         * @static
         * @param {string|object} key 
         * @param {any} value 
         * @memberof App
         */
        static renderData(key, value){
            if('string' == typeof key && 'undefined' != typeof value){
                var el = $('[data-key="'+key+'"]');
                if(el.length > 0){
                    el.html(value);
                }
            }else if ('object' == typeof key){
                for(var kk in key){
                    App.renderData(kk, key[kk]);
                }
            }
        }
    }
    new App;
});