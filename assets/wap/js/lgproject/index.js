/**
 * 2017年11月12日 星期日
 * 全部日志
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){
    var Page = 1;
    class App{
        constructor(){
            this.LoadLogList();
        }
        // 加载日志列表，有效的
        LoadLogList(){
            Wap.ApiRequest('log/search',{page: Page},function (data) {
                if(data.code == 1){
                    var dd = data.data;
                    if(dd.length < 1){
                        return;
                    }
                    var xhtml = '';
                    for(var i=0; i<dd.length; i++){
                        var el = dd[i];
                        xhtml += '<div class="weui-media-box weui-media-box_text" data-no="'+Base64.encode(el.listid+'')+'">' 
                            + '<h4 class="weui-media-box__title">'+el.name+' / '+el.count+'</h4>'
                            + '<p class="weui-media-box__desc">'+el.intro+'</p>'
                            + '<ul class="weui-media-box__info">'
                            +   '<li class="weui-media-box__info__meta">'+el.mtime+'</li>'
                            +   '<li class="weui-media-box__info__meta">'
                            +       '<a href="'+ Wap._wapurl + 'lgproject/edit/item/' + el.listid + '" style="color:#669966;"><i class="fa fa-pencil-square"></i> 编辑</li>'
                            +   '<li class="weui-media-box__info__meta">'
                            +       '<a href="javascript:;" class="js__del text-danger"><i class="fa fa-trash-o"> 删除</i></a></li>'
                            +   '<li class="weui-media-box__info__meta">'
                            +       '<a href="'+ Wap._wapurl + 'lgrecord/edit/item/' + el.listid + '.html"><i class="fa fa-leaf"></i> 写日志</a></li>'
                            +   '<li class="weui-media-box__info__meta">'
                            +       '<a href="'+ Wap._wapurl + 'lgrecord/detail/item/' + el.listid + '.html" class="text-info"><i class="fa fa-eye"></i> 预览</a></li>'
                            + '</ul>'
                            + '</div>'
                            ;
                    }
                    var $xhtml = $(xhtml);
                    // 删除事件绑定
                    $xhtml.find('.js__del').click(function(){
                        var dom = $(this);
                        weui.confirm('您确定要删除此数据列吗?', function(){
                            var no = dom.parents('div.weui-media-box').data('no');
                            Wap.ApiRequest('lgproject/save',{mode:'D','listid':no},function (rdata) {
                                if(rdata.code == 1){
                                    dom.parents('div.weui-panel__bd').remove();
                                    weui.toast(rdata.msg);
                                }
                                else{
                                    weui.alert(rdata.msg);
                                }
                            });
                        });
                    });
                    $('#js__data').html($xhtml);
                    //console.log(data);
                }
            });
        }
    }
    new App();
});