/**
 * 2017年11月12日 星期日
 * 新建日志
 */

import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){
    class Editer{
        constructor(){
            this.DomListener();
        }
        // dom 时间监听器
        DomListener(){
            // 数据保存
            weui.form.checkIfBlur('.js__form');
            $('#submit_lnk').click(function () {
                var loading = Wap.loading();
                weui.form.validate('.js__form', function (error) {
                    if(!error){
                        Wap.ApiRequest('lgproject/save',Wap.formJson('.js__form'),function (data) {
                            loading.hide();
                            if(data.code == -1){
                                weui.alert(data.msg);
                                return null;
                            }
                            Wap.msg_success(data.msg,'日志系统.编辑日志项');
                        });
                    }else{
                        loading.hide();
                    }
                });
            });
        }
    }
    new Editer();
});