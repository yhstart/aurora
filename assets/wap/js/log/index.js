/**
 * 2017年11月12日 星期日
 * 日志系统首页
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){
    var HasLoaded = false;
    class App{
        constructor(){
            this.LoadLogList();
        }
        // 加载日志列表，有效的
        LoadLogList(){
            Wap.ApiRequest('log/search',{'ss':{end_mk: 'N'}},function (data) {
                if(data.code == 1){
                    var dd = data.data;
                    if(dd.length < 1){
                        return;
                    }
                    if(!HasLoaded){
                        HasLoaded = true;
                        $('#active_logs_list').removeClass('aurora-hidden');
                    }
                    var xhtml = '';
                    for(var i=0; i<dd.length; i++){
                        var el = dd[i];
                        xhtml += '<a class="weui-cell weui-cell_access" href="'+ Wap._wapurl + 'lgrecord/detail/item/' + el.listid + '.html">'
                            + '<div class="weui-cell__bd">'
                            + '<p><i class="fa fa-circle text-info"></i> '+el.name+'</p>'
                            + '</div>'
                            + '<div class="weui-cell__ft">' + el.count
                            + '</div>'
                            + '</a>';
                    }
                    $('#active_logs_list').html(xhtml);
                    //console.log(data);
                }
            });
        }
    }
    new App();
});