/**
 * 2017年11月12日 星期日
 * 日志项目
 */
import {
    instance,
    Pager
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){
    var Page = 1,               // 数据列页面
        GPage = 1,              // 分组数据页面
        AllPage,                // 数据列总页数
        GAllPage,               // 分组数据总页数
        CurentNo = false,       // 编辑时当前活动的数据标识
        DdStack = {},           // 数据列堆栈
        GDdStack = [],          // 数据分组堆栈
        Sumy = Wap.getJsVar('sumy'),    // 数据列父表数据
        $forward,                       // 移动对象
        Pid = Wap.getUrlBind('item'),   // 当前数据标识
        GPageLoadedMk = false,          // 分组页面已经加载数据标识
        GPageType     = Sumy.type? Sumy.type: 'M1',    // 分组数据查询类型
        args          = null            // 查询条件
        ;
    var $editor;    // 编辑器对象
    class App extends Pager{
        init(){
            var $instance = this;
            this.LoadLogList();
            this.domListener();
            // 加载为edit页面是=时，如果没有id直接返回到主页
            this.onWhenLoad('edit', function(){
                if(!CurentNo){
                    $instance.router('main');
                }
            });
            // 分组页面
            this.onWhenLoad('group', function(){
                if(!GPageLoadedMk){
                    $instance.loadGroupList();
                    GPageLoadedMk = true;
                }
            });
            // 参数
            this.options = {};
        }
        // 数据加载
        LoadLogList(){
            var borderXhtml = '<div class="weui-flex aurora-border"></div>';
            var $instance = this;
            Wap.ApiRequest('lgrecord/data',{page: Page, item: Pid, args: args},function (data) {
                if(data.code == 1){
                    var xhtml = '';
                    var result = data.data;
                    var dd = result.result;
                    // 总列数计算
                    if(!AllPage || Page == 1) AllPage = Wap.getAllPage(result.count,result.num);
                    for(var i=0; i<dd.length; i++){
                        var el = dd[i];
                        DdStack[el.no] = el;
                        xhtml += (xhtml? borderXhtml: '') //'<setion>'
                            + '<h2 class="title">'+(el.subhead? el.subhead + '/':'')+el.date+'</h2>'
                            + '<setion class="log_content">'+el.content
                            +   '<p class="text-right js__lnks" data-no="'+el.no+'">'
                            +       '<a href="javascript:;" class="delete_lnk text-danger">删除</a> '
                            +       '<a href="javascript:;" class="to_edit_page">编辑</a> '
                            +   '</p>'
                            + '</setion>'
                            // + '<setion>'
                        ;
                    }
                    // console.log(dd)
                    var $xhtml = $(xhtml);
                    // 编辑页面跳转
                    $xhtml.find('.to_edit_page').click(function(){
                        var dom = $(this);
                        var no = dom.parent('p.js__lnks').data('no');
                        CurentNo = no;
                        var el = DdStack[no];
                        $('#js__editor').html(el.content);  
                        if($editor){
                            $editor.restore();
                        }
                        $editor = textboxio.replace('#js__editor', {
                            ui: {
                                toolbar: {
                                    // Define which items you'd like in the toolbar here,  and their
                                    // ordering. See http://docs.ephox.com/display/tbio/Command+Item+IDs 
                                    // for a list of available commands
                                    items: ['undo', 'insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
                                }
                            }
                        });                      
                        $instance.router('edit');
                    });
                    // 删除确认
                    $xhtml.find('.delete_lnk').click(function(){
                        var dom = $(this);
                        var no = dom.parent('p.js__lnks').data('no');
                        weui.confirm('您确定要删除此日志记录吗?', function(){
                            Wap.ApiRequest('lgrecord/save',{mode:'D','no':Base64.encode(no)},function (rs) {
                                if(rs.code == 1){
                                    // 删除有问题
                                    var setion = dom.parents('setion');
                                    var h2 = setion.prev('h2.title');
                                    var div = setion.next('div.aurora-border');
                                    setion.remove();
                                    h2.remove();
                                    div.remove();
                                }
                                weui.alert(rs.msg);
                            });
                        });
                    });
                    if(Page == 1){
                        $('#js__log_list').html($xhtml);
                    }else{
                        $('#js__log_list').append($xhtml);
                    }
                    
                }
                else{
                    weui.alert(data.msg);
                }
            });
        }
        // 加载分组数据
        loadGroupList(){
            var $instance = this;
            Wap.ApiRequest('lgrecord/group',{page: GPage, type: GPageType, 'pid': Pid},function (rs) {
                if(rs.code == 1){
                    var data = rs.data;
                    var list = data.rs;
                    var html = '';
                    if(!GAllPage || GPage == 1) GAllPage = Wap.getAllPage(data.count,data.num);
                    for(var i=0; i<list.length; i++){
                        var el = list[i];
                        var idx = GDdStack.push(el) - 1;
                        html += '\
                            <a class="weui-cell weui-cell_access" href="javascript:;" data-no="'+idx+'">  \
                                <div class="weui-cell__bd">                             \
                                    <p>'+el.item+'</p>                                  \
                                </div>                                                  \
                                <div class="weui-cell__ft">                             \
                                '+el.count+'                                            \
                                </div>                                                  \
                            </a>                                                        \
                            '
                        ;
                    }
                    if(GPage > 1){
                        $('#js__group .js__data').append(html);
                    }else{
                        $('#js__group .js__data').html(html);
                    }
                    // 详情跳转
                    $('#js__group .js__data a')/*.find('a')*/
                        .off('click').on('click', function(){
                            var no = $(this).data('no');
                            var el = GDdStack[no];
                            if(!el){
                                return null;
                            }
                            args = {t:'GROUP', c: GPageType, v: el.item};
                            Page = 1;
                            $instance.LoadLogList();                     
                            $instance.router('main');
                    });
                }
            });
        }
        // 事件监听
        domListener(){
            var $instance = this;
            // 保存编辑器
            $('#submit_lnk').click(function(){
                if(CurentNo && $editor){                    
                    var saveData = {
                        content: $editor.content.get(),
                        no: Base64.encode(CurentNo)
                    };
                    //console.log(saveData);
                    Wap.ApiRequest('lgrecord/save', saveData, function(rs){
                        weui.alert(rs.msg);
                    });
                }
                else{
                    weui.alert('无法保存数据，您可能还没有更改数据');
                }
            });
            var hasTipNoMoreDataMk = false;
            // 滚动到底部，自动翻页
            // main页面
            var loadSecLimiter = 0; // 加载间隔，方式一次滚动加载许多次
            $('#js__main').on('scroll', function(){
                var scrollTop = this.scrollTop;
                var $pageBar = $('#js__main').find('.page-bar');
                if($pageBar.length > 0){
                    var pageBar = $pageBar[0];
                    // console.log(pageBar.clientHeight, pageBar.offsetHeight, pageBar.offsetTop, this.scrollTop);
                    if(pageBar.offsetTop < scrollTop){
                        if(!$pageBar.hasClass('top-fixed')){
                            $pageBar.addClass('top-fixed');
                            // $('#js__main .js__title').html((Sumy.name || '日志系统')+' / '+ Page);
                        }
                        $('#js__main .js__title').html(Page+' / '+ AllPage +' '+ (Sumy.name || '日志系统'));
                        $forward.show();
                    }else{
                        if($pageBar.hasClass('top-fixed')){
                            $pageBar.removeClass('top-fixed');
                            $('#js__main .js__title').html('');
                        }
                        $forward.hide();
                    }
                }
                if(this.scrollHeight - scrollTop -50 <= this.clientHeight){
                    if(Page >= AllPage){
                        if(!hasTipNoMoreDataMk){
                            weui.topTips('没有更多数据了');
                            hasTipNoMoreDataMk = true;
                        }
                        return false;
                    }
                    if(loadSecLimiter != 0){
                        setTimeout(function(){
                            loadSecLimiter = 0;
                        },3000);   
                        return false;
                    }
                    Page += 1;
                    $instance.LoadLogList();
                    loadSecLimiter = 3;
                    
                }
            });
            $forward = Wap.forwardTopScroll('#js__main');
            $forward.hide();

            // 滚动到底部，自动翻页
            // 分组页面
            var gLoadSecLimiter = 0; // 加载间隔，方式一次滚动加载许多次
            var GroupHtnmdMk = false;
            $('#js__group').on('scroll', function(){
                var scrollTop = this.scrollTop;
                if(this.scrollHeight - scrollTop -50 <= this.clientHeight){
                    if(GPage >= GAllPage){
                        if(!GroupHtnmdMk){
                            weui.topTips('没有更多数据了');
                            GroupHtnmdMk = true;
                        }
                        return false;
                    }                
                    if(gLoadSecLimiter != 0){
                        setTimeout(function(){
                            gLoadSecLimiter = 0;
                        },3000);   
                        return false;
                    }    
                    GPage += 1;
                    $instance.loadGroupList();
                    gLoadSecLimiter = 3;
                }
            });
            
            // 搜索
            $('#super_search_lnk').click(function(){
                var pdom = $('#js__filter');
                var value = pdom.find('[name="search_value"]').val();
                var col = pdom.find('[name="search_col"]')[0].value;
                var equal = pdom.find('[name="search_equal"]')[0].value;
                args = {e:equal, c: col, v: value};
                Page = 1;
                $instance.LoadLogList();                     
                $instance.router('main');
            });

            // 查询分组
            $('#use_group_lnk').click(function(){
                GPageType = $instance.getGroupByDom();
                GPage = 1;
                $instance.loadGroupList();
                $instance.router('group');
            });
            // 保存分组
            $('#save_group_lnk').click(function(){
                GPageType = $instance.getGroupByDom();
                Wap.ApiRequest('lgproject/save',{type: GPageType, listid: Base64.encode(Sumy.listid+'')},function (data) {
                    if(data.code == -1){
                        weui.topTips('分组保存失败！');
                        return null;
                    }
                    weui.toast('分组保存成功！');
                });                
            });
            // 分组自动设置
            if(Sumy.type){
                var $gTypeEl = $('[value="'+Sumy.type+'"]');
                if($gTypeEl.length > 0){
                    $gTypeEl.attr("checked", true);
                }
            }
        }
        getGroupByDom(){
            return $('[name="setting_group"]:checked').val();
        }
    }
    new App();
});