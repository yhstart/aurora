/**
 * 2017年11月12日 星期日
 * 日志编写
 */

import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){
    var $editor;
    class App{
        constructor(){
            this.DomListener();
        }
        DomListener(){
            // 数据保存
            weui.form.checkIfBlur('.js__form');
            $('#submit_lnk').click(function () {                
                var loading = Wap.loading();
                // 从编辑器中更新内容
                if($editor){
                    $('#content_ipter').val($editor.content.get());
                }
                weui.form.validate('.js__form', function (error) {
                    if(!error){
                        Wap.ApiRequest('lgrecord/save',Wap.formJson('.js__form'),function (data) {
                            loading.hide();
                            if(data.code == -1){
                                weui.alert(data.msg);
                                return null;
                            }
                            Wap.msg_success(data.msg,'日志系统.编辑日志');
                        });
                    }else{
                        loading.hide();
                    }
                });
            });
            // 内容编辑
            $editor = textboxio.replace('#content_ipter', {
                ui: {
                    toolbar: {
                        // Define which items you'd like in the toolbar here,  and their
                        // ordering. See http://docs.ephox.com/display/tbio/Command+Item+IDs 
                        // for a list of available commands
                        items: ['undo', 'insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools']
                    }
                }
            });   
        }
    }
    new App();
});