/**
 * 2017年10月22日 星期日
 * 财务动态报表
 * 基本标标准化： 使用 js class App, 内部全局变量采用大小开头， App 内部对象使用小写开头
 */

import {
    instance,
    Pager
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){    
    var $storage = Wap.storage().table('wap_finance_report');
    var Type = Wap.getUrlBind(Wap.Think().request('action'));
    var Value = Type? Wap.getUrlBind(Type): $storage.get('dynamic_no', null);
    if(!Type && Value){
        switch(Value.length){
            case 4:
                Type = 'year';
                break;
            case 6:
                Type = 'month';
                break;
        }
    }
    var Main = {};
    /**
     * 数据列表页面
     * @class PageList
     */
    class PageList{
        constructor(){}
    }
    /**
     * 系统APP
     * @class App
     */
    class App extends Pager{
        init(){
            // 获取指定的数据对象
            if(Value){
                // console.log(Value);
                this.navId = 'detail';
                this.dynamicDataReport();
            }else{
                this.getAccessDataList();
            }
            // 主页面加载时
            this.onWhenLoad('main', () =>{
                if(!Main.firstLoad){
                    this.getAccessDataList();
                    Main.firstLoad = true;
                }
            });
        }
        /**
         * 获取可查看的数列表
         * @memberof App
         */
        getAccessDataList(){
            var $instance = this;
            Wap.ApiRequest('freport', null, function(rs){
                // console.log(rs);
                if(rs.code == '1'){
                    var xhtml = '';
                    var data = rs.data;
                    for(var k in data){
                        var el = data[k];
                        k = k.substr(0, 4);
                        xhtml += '<a class="weui-cell weui-cell_access" href="javascript:;">' +
                            '<div class="weui-cell__bd">' +
                            '<p><i class="fa fa-minus-circle text-primary"></i> '+k+
                                ' : <span class="text-success">'+el.money+'</span></p>' +
                            '</div>' +
                            '<div class="weui-cell__ft">'+el.count+'</div>' +
                            '</a>'
                        ;
                        var months = el.month;
                        var childCellHtml = '';
                        for(var mth in months){
                            var elM = months[mth];
                            childCellHtml += '<a class="weui-cell weui-cell_access js__month_lnk" href="javascript:;" data-no="'+mth+'">' +
                                '<div class="weui-cell__bd" style="margin-left: 50px;">' +
                                '<p><i class="fa fa-circle-o text-info"></i> '+mth+' : <span class="text-success">'+elM.money+'</span></p>' +
                                '</div>' +
                                '<div class="weui-cell__ft">'+elM.count+'</div>' +
                                '</a>'
                            ;
                        }
                        childCellHtml += '<a class="weui-cell weui-cell_access js__year_lnk" href="javascript:;" data-no="'+k+'">'+
                            '<div class="weui-cell__bd" style="margin-left: 50px;">' +
                            '<p><i class="fa fa-circle-o text-info"></i> 查看年'+k+'度报表</p>' +
                            '</div>' +
                            '<div class="weui-cell__ft"></div>' +
                            '</a>';
                        xhtml += '<div class="weui-cells">' + childCellHtml+ '</div>';
                    }
                    var $cell = $(xhtml);
                    // 月度详情查询
                    $cell.find('.js__month_lnk').click(function(){
                        var dataNo = $(this).attr('data-no');
                        var dd = months[dataNo];
                        $instance.dynamicDataReport('month', dataNo);
                        // console.log(dd);
                    });
                    // 年度详情查询
                    $cell.find('.js__year_lnk').click(function(){
                        var dataNo = $(this).attr('data-no');                        
                        $instance.dynamicDataReport('year', dataNo);
                    });
                    $('#js__main > .top-cells').html($cell);
                }
            });
        }
        /**
         * 通过获取获取动态执行生成的财务报表
         * @memberof App
         */
        dynamicDataReport(type, value){
            type = type || Type;
            value = value || Value;
            if(value){
                $storage.update('dynamic_no', value);
            }else{
                value = $storage.get('dynamic_no');
            }
            if(!value){
                return;
            }
            var $instance = this;
            var typeMap = {'month': '月度', 'year': '年度'};
            $('#js__detail .page__title').html(value + typeMap[type] + '财务报表');
            Wap.ApiRequest('freport/dynamic', {type, value}, function(data){
                // console.log(data);
                //if(data.)
                $instance.router('#js__detail');
                $instance.detailDataRender(data.data);
            });
        }
        /**
         * detail 页面详情，自动设置
         * @param {string|JSON} key 
         * @param {any} value 
         * @memberof App
         */
        detailDataRender(key, value){
            if('object' == typeof key){
                for(var k in key){
                    this.detailDataRender(k, key[k]);
                }
            }
            else if(key && 'undefined' != typeof value){
                var dom = $('#js__detail').find('[data-col="' + key +'"]');
                if(dom.length > 0){
                    dom.html(value);
                }
            }
        }
    }
    new App();
});