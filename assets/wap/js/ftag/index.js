/**
 * Created by Administrator on 2017/6/17 0017.
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function () {
    var argv = null;
    function loadData(page) {
        page = (!page || isNaN(page))? 1: page;
        var loading = instance.weuiLoading('数据正在加载中……');
        Wap.ApiRequest('finance/get_tags',{page:page},function (rdata) {
            loading.hide();
            if(rdata.code == 1){
                var xhtml = '';
                var data = rdata.data;
                var result = data.result;
                for(var i=0; i<result.length; i++){
                    var el = result[i];
                    var icon = (el.private_mk == 'Y')? '<i class="fa fa-user-o text-info"></i>':'<i class="fa fa-globe text-success"></i>';
                    xhtml += '<a class="weui-cell'+(el.private_mk == 'Y'? ' weui-cell_access':'')+'" href="javascript:;">'
                          + '<div class="weui-cell__bd">'
                          + '<p>'+icon + ' ' +el.tag+'</p>'
                          + '</div>'
                          + '<div class="weui-cell__ft" style="font-size: 0.78em;">'+(el.private_mk == 'Y'? '':el.author+ '/')+el.date+'</div>'
                          + '</a>'
                    ;
                }
                $('#home > .weui-cells').append(xhtml);
                argv = {page:data.page,all_page:data.all_page};
            }
            else weui.topTips(rdata.msg);
        });
    }
    /**
     * 数据加载更多
     */
    function nextPage(){
        if(!argv) return weui.topTips('无法获取数据！');
        if(argv.page < argv.all_page){
            loadData(argv.page + 1);
        }else{
            weui.topTips('没有更多数据！');
        }
    }
    // 滑动特效
    function panelSwipe(dom) {
        var id = dom.attr('id');
        dom.toggleClass('aurora-hidden');
        var targetId = '#' + (id == 'home'? 'edit':'home');
        $(targetId).toggleClass('aurora-hidden');
    }
    // 右滑动时返回
    Wap.SwipeRightBack();
    
    var $panel = $('.weui-tab__panel');
    // tag 滑动切换 -> 查看与新增
    $panel.swipeLeft(function () {
        panelSwipe($(this));
    });

    /**
     * 自动适应描点
     * @param hash
     */
    function AdjustRequest(hash) {
        hash = hash? hash : location.hash;
        if(hash){
            var curTab = $(hash);
            if(curTab.length > 0 && curTab.attr('class').indexOf('aurora-hidden') > -1){
                $('.weui-tab__panel').addClass('aurora-hidden');
                curTab.removeClass('aurora-hidden');
            }
            location.hash = hash;
        }
    }
    AdjustRequest();
    // tab 页面跳转
    $('.js__goto').click(function () {
       var id = $(this).attr('href');
        if(id) AdjustRequest(id);
        //console.log(id);
    });
    // 编辑页面保存
    $('#edit_submit_lnk').click(function () {
        var tag = $('#tag_ipter').val();
        if(tag == ""){
            weui.topTips('请设置标签!',2000);
            return;
        }
        var loading = instance.weuiLoading('数据提交中...');
        var privateMk = $('#prvtmk_ipter').is(':checked')? 'N':'Y';
        var saveData = {tag:tag,private_mk:privateMk};
        Wap.ApiRequest('finance/tag_save',saveData,function (rdata) {
            loading.hide();
            if(rdata.code == -1){
                weui.alert(rdata.msg);
                return;
            }
            weui.toast(rdata.msg);
            // 表单重置
            $('#tag_ipter').val('');
            $('#prvtmk_ipter').attr('checked',true);
            AdjustRequest('#home');
        });
    });
    loadData();
    // (上拖)上滑动 - 数据翻页
    $('.weui-footer').swipeUp(function () {
        nextPage();
    });
    
    // 加载到底部时自动翻页
    $('a.weui-footer__link').click(function () {
        nextPage();
    });
});