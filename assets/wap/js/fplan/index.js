/**
 * 2017年10月29日 星期日
 * 财务计划-js
 * 程序接口化，将原来的php渲染转化 ajax API化
 */
import {
    instance
} from '../../../src/Wap'
// 系统全局性变量
window.Wap = instance

$(function(){
    let page = 1;
    let maxPage;
    // 页面主方法
    class App{
        constructor(){
            // 加载到底部时自动翻页
            let $this = this;
            $('a.weui-footer__link').click(function () {
                if(page < maxPage){
                    page += 1;
                    $this.loadData();
                }else{
                    weui.topTips('没有更多数据了，朋友！');
                }
            });
            this.loadData();
        }
        // 加载数据
        loadData(){
            Wap.ApiRequest('fplan/sets',{page},function (rdata) {
                if(rdata.code == 1){
                    //console.log(rdata);
                    var dd = rdata.data;
                    maxPage = Wap.getAllPage(dd.count, dd.num);
                    var rs = dd.result;
                    var xhtml = '';
                    for(var k in rs){
                        var el = rs[k];
                        var endMk = el.end_mk;
                        xhtml += 
                        '<div class="weui-panel__bd">' +
                            '<div class="weui-media-box weui-media-box_text">' +
                                '<h4 class="weui-media-box__title">' + 
                                    '<i class="fa fa-clock-o' + (endMk == 'Y'? '':' text-success') +'"></i>  '+ el.plan + (el.ctt? ' <span class="weui-badge">'+ el.ctt+'</span>':'') +
                                '</h4>' +
                                '<p class="weui-media-box__desc">添加时间 '+ el.mtime + '</p>' +
                                '<ul class="weui-media-box__info">' +
                                    '<li class="weui-media-box__info__meta"><a href="'+Wap.getWapUrl('fplan/edit/item/'+el.plan_no, true)+'">编辑</a></li>' +
                                    (endMk == 'N' && el.cycle? 
                                        ('<li class="weui-media-box__info__meta">' +
                                            '<a href="'+Wap.getWapUrl('fplan/schedule.html?src='+Base64.encode(el.plan_no))+'">财务排期</a>' +
                                        '</li>')
                                    :'') + 
                                    '<li class="weui-media-box__info__meta">' +
                                        '<a href="'+Wap.getWapUrl('fplan/account_bind/item/'+el.plan_no, true)+'">账单绑定</a>' +
                                    '</li>' +
                                '</ul>' +
                            '</div>' +
                            '<div class="weui-flex aurora-border"></div>' +
                        '</div>'
                        ;
                    }
                    if(xhtml){
                        var $html = $(xhtml);
                        $('#js__data').append($html);
                    }else{
                        html = 
                        `<div class="weui-cells" style="min-height: 420px;">
                            <a class="weui-cell weui-cell_access" href="`+Wap.getWapUrl('fplan/edit', true)+`">
                                <div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>
                                <div class="weui-cell__ft">
                                    您还没一条财务记录
                                </div>
                            </a>
                        </div>`;
                        var $html = $(xhtml);
                        $('#js__data').html($html);
                    }                    
                }
                else{
                    weui.alert(rdata.msg);
                }
            });
        }
    }
    // 调用方法
    new App();
});