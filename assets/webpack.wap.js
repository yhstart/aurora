/**
 * wap 项目打包配置
 * 2017年12月27日 星期三
 */

const {
    AuroraJsPacker,
    nodeEnv
}  = require('./webpack.config')



var options = {
    baseDir: 'wap/js/'
}
var FileQueue = (new AuroraJsPacker(options)).js([
    'index/index',
    'article/index',
    'article/edit',

    'faccount/index',
    'faccount/edit',
    'faccount/detail',
    'faffair/index',
    'faffair/edit',
    'feekback/index',
    'feekback/edit',
    'fevent/index',
    'fevent/edit',
    'fplan/index',
    'fplan/edit',
    'fplan/account_bind',
    'freport/dynamic',
    'fsubject/index',
    'ftag/index',
    'finance/index',

    'graffiti/edit',
    'lgproject/index',
    'lgproject/edit',
    'lgrecord/detail',
    'lgrecord/edit',
    'log/index',
    'login/index',
    'register/index',
    'user/edit',
    'user/password',
    'user/portrait',
    
    'verify/index.ts',
    'ref_account/index.ts',
])
.getFiles()

//console.log(FileQueue)

module.exports = FileQueue