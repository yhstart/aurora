/**
 * 2017年12月26日 星期二
 * 系统自动登录实现
 */
var Wap,
    strgSn,
    strg
;
class AutoLogin{
    /**
     * 
     * @param {Wap} instance  Wap 实例
     */
    constructor(instance){
        Wap = instance;
        strgSn = Wap.storage().table('_aurora_login');      // seesionStorage
        strg = Wap.storage('local')                         // localStorage
                .table('_aurora_login')
                ;
        // console.log(strg.get('rmbr_ftoken'), strg.get('account'));
        this.token = strg.get('rmbr_ftoken');
        this.account = strg.get('account');
    }
    /**
     * 登录系统
     * @param {function} callback 
     */
    loging(callback, noHasLogin){
        // 有session时视为手动退出，此时不再自动登录
        if(strgSn.get('rmbr_ftoken') && strgSn.get('account')){
            return null;
        }
        noHasLogin = noHasLogin? true: false;
        if(this.account && this.token){       
            var token = this.token;
            var account = this.account;
            strgSn.add({rmbr_ftoken: token, account: account});
            var loading = false;
            if(noHasLogin){
                loading = this.weuiLoading('系统正在自动登录……');
            }
            Wap.ApiRequest('login/auto_auth',{token, account},function (rdata) {
                if(loading){
                    loading.hide();
                }
                if(callback){
                    callback(rdata);
                }
                // 加载成功以后刷新页面
                location.reload(true);
            });
        }
    }
    /**
     * 用户数据更新
     * @param {Object} data 
     */
    add(data){
        if(data && 'object' == typeof data){
            strg.add(data)
        }        
    }
}
export default AutoLogin