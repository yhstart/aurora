/**
 * 2017年12月26日 星期二
 * 模块页面, 实现 tab 自切换，以及加载是的监听
 * @class Pager
 */
class Pager{
    constructor(){
        // 后去当前的锚点
        var hash = location.hash;     
        this.navId = hash? hash.substr(1): 'main'; 
        // 载入系统初始化函数
        this.init();
        // 路由开始
        this.router();
        // 通用对象监听
        // 私有接口
        this._domListener();
    }
    /**
     * 系统初始化接口
     * @memberof Pager
     */
    init(){}
    /**
     * 系统路由
     * @param {string|null} name 
     * @memberof Pager
     */
    router(name){  
        name = name || this.navId;
        var id;
        if(0 === name.indexOf('#')){
            id = name;
        }else if(name.indexOf('js__') > -1){
            id = '#'+name;
        }else{
            id = '#js__' + name;
        }
        var dom = $(id);
        if(dom.length > 0){
            $('.weui-tab').css({'display':'none'});
            $(id).css({'display':null});
            var hash = id.replace('js__', '');
            // 事件监听
            this.onWhenLoad(hash.substr(1));
            location.hash = hash;
        }
    }
    /**
     * 模块加载时间监听时间
     * @param {string} name 
     * @param {function} callback 
     * @returns {bool}
     * @memberof Pager
     */
    onWhenLoad(name, callback){
        name = name? '__event_'+name: false;
        if(name && callback){
            if('function' == typeof callback){
                this[name] = callback;
                return true;
            }
            return false;
        }else if(name && this[name]){
            this[name]();
        }
    }
    /**
     * 文档对象监听器，实现私有接口
     * @memberof Pager
     */
    _domListener(){
        var $instance = this;
        $('.tab__lnk').click(function(){
            // var target = $(this).attr('data-target');
            var target = $(this).data('target');
            $instance.router(target);
        });
    }
}

export default Pager