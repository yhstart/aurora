/**
 * 系统程序配置
 * 2017年12月26日 星期二
 */

/**
 * 系统配置程序
 */
var Config = {
    baseurl: '/',                     // 地址web 地址
    AjaxTimeout: 2                    // ajax 超时请求设置分钟
}

export {
    Config
}