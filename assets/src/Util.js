/**
 * 2017年12月26日 星期二
 * web/wap 通用程序工具栏
 * 提供Aurora公共语言包程序
 */

/**
 * getQuery 获取 json
 * @param skey 键值
 * @return {json/string}
 */
function getQuery(skey) {
    var json = {};
    var queryStr = location.search;
    if(queryStr.length>0){
        queryStr = queryStr.substr(1);
        var tmpArray = queryStr.split('&');var key,value,idx;
        for(var k=0;k<tmpArray.length;k++){
            value = tmpArray[k];
            idx = value.indexOf('=');
            // 删除非法字符
            if(idx == -1) continue;
            key = value.substr(0,idx);
            value = value.substr(idx+1);
            if(skey && key == skey) return value;   // 获取单值
            json[key] = value;
        }
    }
    if(skey) return "";
    return json;
}
/**
 * 系统工具级类
 */
class Util{
    constructor(){}
    /**
     * string 类型判断
     * @param {any} value 
     * @returns {boolean}
     */
    is_string(value){
        if(typeof(value) == 'string') return true
        return false
    }
    /**
     * object 类型判断
     * @param {any} value 
     * @returns {boolean}
     */
    is_object(value){
        if(typeof(value) == 'object') return true
        return false
    }
    /**
     * 查看array/json 是否存在值
     * @param {*} key 
     * @param {array} arr 
     * @returns {boolean}
     */
    inArray(key,arr){
        var ret = false
        if(key && this.is_object(arr)){
            if(this.is_array(arr)){
                for(var k=0; k<arr.length; k++){
                    if(arr[k] == key) return true
                }
            }
            else{
                for(var k in arr){
                    if(arr[k] == key) return true
                }
            }
        }
        return false
    }
    /**
     * 求数组或json的长度
     * @param {*} value 
     */
    objectLength(value){
        var len = 0
        // 对象长度
        if(this.is_object(value)){
            if(this.is_array(value)) return value.length
            // json 通过遍历获取
            for(var k in value){
                len = len + 1
            }
            return len
        }
        // 其他具有length 属性的对象
        else if(value && value.length) return value.length
        return len
    }

    /**
     * @param {*} value
     * @returns {boolean}
     */
    is_function(value){
        if(typeof(value) == 'function') return true
        return false
    }
    /**
     * @param {*} value 
     */
    is_number(value){
        var peg = /^[0-9]+$/
        if(value){
            value = value.replace(/\s/g,'') //  删除空格
            return peg.test(value)
        }
        return false
    }
    /**
     * @param {*} value 
     */
    undefind(value){
        if(typeof(value) == 'undefined') return true
        return false
    }
    /**
     * @param {*} value 
     */
    empty(value){
        if(this.undefind(value)) return true
        else if(value == '') return true
        // else if(value == 0) return true; // "00" 无法通过
        else if(value == null) return true
        return false
    }
    
    /**
     * @name 获取 url-中query的值 如： ?k1=v1&k3=v3 => its100a.getQuery('k1') => 'v1'
     */
    getQuery(key){
        var strKey = key+'=';
        var href = location.href;
        var idx = href.indexOf(strKey,href);
        if(idx == -1) return '';
        var tmpStr = href.substr(idx);
        if(tmpStr.indexOf('&',tmpStr) > -1) tmpStr = tmpStr.substr(0,tmpStr.indexOf('&',tmpStr));
        tmpStr = tmpStr.substr(tmpStr.indexOf('=',tmpStr)+1);
        return tmpStr;
    }
    urlGet(key) {
        return getQuery(key);
    }
    /**
     * js json转化get请求参数
     * @param json
     * @returns {string}
     */
    parseQuery(json) {
        var queryString = '';
        var tmpArr = [];
        for(var k in json){
            tmpArr.push(k+'='+json[k]);
        }
        if(tmpArr.length>0) queryString = '?'+tmpArr.join('&');
        return queryString;
    }
    updateQuery(json,url) {
        json = fn.JsonMerge(getQuery(),json);
        var querystr = fn.parseQuery(json);
        url = url? url:(location.origin+location.pathname+querystr+location.hash);
        return url;
    }
    /**
     * 获取 hash 值
     * @param {string|null} url 
     */
    getHash(url){
        url = url? url: location.href
        var hash = null
        var idx
        if((idx = url.indexOf('#')) > -1){
            hash = url.substr(idx+1)
            if((idx = hash.indexOf('?')) > -1){
                hash = hash.substr(0, idx)
            }
        }
        return hash
    }
    /**
     * array 合并
     * @param array ...
     */
    ArrayMerge() {
        if(arguments.length < 2) return [];
        var baseArray = arguments[0];
        var tmpArray = [];
        for(var k=1; k<arguments.length; k++){
            tmpArray = arguments[k];
            if(typeof(tmpArray) == 'object' && tmpArray.length > 0) {
                for (var kk = 0; kk < tmpArray.length; kk++) {
                    baseArray.push(tmpArray[kk]);
                }
            }
        }
        return baseArray;
    };
    /**
     * json 合并
     * @constructor
     */
    JsonMerge() {
        if(arguments.length < 2) return [];
        var baseJson = arguments[0]; var tmpJson;
        baseJson = (typeof baseJson == 'object' && typeof (baseJson.length) == 'undefined')? baseJson:{};
        for(var k=1;k<arguments.length;k++){
            tmpJson = arguments[k];
            if(typeof(tmpJson) == 'object' && typeof(tmpJson.length) == 'undefined'){
                for(var kk in tmpJson){
                    baseJson[kk] = tmpJson[kk];
                }
            }
        }
        return baseJson;
    }
    /**
     * json 数据清洗 -> 根据keys帅选数据
     * @param json
     * @param keys string/[]string
     * @param notnull 是否清楚其中的空值->
     * @constructor
     */
    JsonClear(json,keys,notnull) {
        // 去除空值
        if(notnull){
            var tmpJson = {};
            for(var k in json){
                if('' == json[k] || null == json[k]) continue;
                tmpJson[k] = json[k];
            }
            json = tmpJson;
        }
        if($.isArray(keys)){
            for(var i=0; i<keys.length; i++){
                var k = keys[i];
                if(typeof json[k] != 'undefined') delete json[k];
            }
        }else if(typeof json[keys] != 'undefined'){
            delete json[keys];
        }
        return json;
    }
}

export default Util