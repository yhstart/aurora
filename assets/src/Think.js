/**
 * ThinkPhp 前端配合插件
 * 2017年12月26日 星期二
 */

// 框架级获取提，如 模块名/控制器名称/方法名等
var _ThinkRequest = {};
var __baseurl;
function thinkQuery(idx) {
    if(!_ThinkRequest.parseArray){
        var pathname = location.pathname.replace(__baseurl,'');
        pathname = pathname.replace('.html','');
        _ThinkRequest.pathname = pathname;
        var tmpArray = pathname.split('/');
        _ThinkRequest.parseArray = tmpArray;
    }
    if(_ThinkRequest.parseArray){
        if(idx || idx === 0){
            if(idx < _ThinkRequest.parseArray.length) return _ThinkRequest.parseArray[idx];
            else if(idx>0) return 'index';
        }
    }
}

class Think{
    /**
     * @param {Aurora} Obj 获取相关的配置文件
     */
    constructor(Obj){
        this.Obj = Obj
        __baseurl = this.Obj._baseurl
        thinkQuery()
        this.location = _ThinkRequest
    }
    /**
     * 获取控制器相关名称
     * @param key
     * @returns {*}
     * @constructor
     */
    request(key) {
        if(_ThinkRequest[key]) return _ThinkRequest[key];
        else{
            switch (key){
                case 'module':
                    _ThinkRequest.module = thinkQuery(0);
                    break;
                case 'controller':
                    _ThinkRequest.controller = thinkQuery(1);
                    break;
                case 'action':
                    _ThinkRequest.action = thinkQuery(2);
                    break;
            }
        }
        if(_ThinkRequest[key]) return _ThinkRequest[key];
    }
    getUrlByAction(action) {
        return __baseurl
            + this.request('module')
            + '/'
            + this.request('controller')
            + '/'
            + action;
    }
}

export {Think}