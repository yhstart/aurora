/**
 * 桌面浏览器端js公共脚本程序
 * 2017年12月26日 星期二
 */
import {Aurora} from './Aurora'
import AutoLogin from './web/AutoLogin'

import FormListener from './web/FormListener'
import Form from './Form'
import  Bootstrap from './web/Bootstrap'

class Web extends Aurora{
    // constructor(){
    //     super()
    // }
    /**
     * 获取单个表单的值，扩展 $.val 函数使 input/select/textare 等透明
     * @param selector string|jquery
     * @param isJson 是否返回 k-v json 值
     * @return json|string
     */
    formValue(selector,isJson) {
        var el = (typeof selector == 'object')? selector:$(selector);
        var value = '';
        if(el.is('input')){
            var type = el.attr('type');
            if(type == 'checkbox' || type == 'radio') {
                if (el.is(':checked')) value = el.val();
            }
            else value = el.val();
        }
        else if(el.is('textarea')) value = el.val();
        else if(el.is('select')){
            value = el.find('option:selected').val();
        }
        if(isJson){
            var name = el.attr('name');
            var tmpJson = {};
            tmpJson[name] = value;
            return tmpJson;
        }
        return value;
    }
    /**
     * 通过选择器获取表单值
     * @param selector []string|string *
     * @return json
     */
    getDataBySel(selector) {
        selector = (typeof selector == 'object')? selector:[selector];
        var saveData = {};
        for(var i=0; i<selector.length; i++){
            var el = $(selector[i]);
            var name = el.attr("name");
            if(name == '') continue;
            saveData[name] = this.formValue(el);
        }
        return saveData;
    }
    /**
     * 删除自动提交，点击删除链接即可
     * @param selector
     * @param data
     */
    indexDelLink(selector,data) {
        selector = selector? selector:'.js__del_lnk';
        data = 'data-'+(data? data:'id');
        var el = $(selector);
        if(el.length > 0){
            el.click(function () {
                var pk = $(this).attr(data);
                var json = {mode:'D',pk:pk};
                Web.confirm('您确定要删除数据吗？',function () {
                    Web.post(Web.Think().getUrlByAction('save'),json);
                });
            });
        }
    }
    // 需要引入 tinymce 用于统一 富文本样式
    tinymce(selector){
        tinymce.init({
            selector: selector,
            // plugins: [
            //     'advlist autolink lists link image charmap print preview anchor',
            //     'searchreplace visualblocks code fullscreen',
            //     'insertdatetime media table contextmenu paste code textcolor'
            // ]
            height: 300,
            //theme: 'modern',
            plugins: [
                'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'save table contextmenu directionality emoticons template paste textcolor'
            ]
            ,toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview fullpage | forecolor backcolor emoticons'
            // ,theme: 'modern'
            ,skin: 'lightgray'
            // 汉化
            ,language:'zh_CN'
        });
    }
}

// 数据实例化对象
var instance = new Web()

/**
 * @return {object} => {this.loging/this.add}
 */
instance.autoLogin = function(){
    return new AutoLogin(instance)
}

// 表单处理项
instance.Extends(Form)

// 系统全局变量
window.FormListener = FormListener

// Bootstrap 插件
Bootstrap(instance)

// export default {
//     instance,
//     t1: instance
// }

export {instance}
