/**
 * 2017年12月26日 星期二
 * Wap/Web form 处理器
 */
var Form = {
    // js/模拟保单----------------------------------------------------------------------->
    form: function(url,data,method){
        if(this.empty(url) || this.empty(data)) return false;
        if(this.is_string(data)){
            try {
                data = JSON.parse(data);
            } catch (error) {
                return error;
            }
        }
        if(method) method = method.toLowerCase();
        method = this.empty(method)? 'post':method;
        if(method && (method != "post" && method != "get")) method = "post";
        var form = document.createElement("form");
        form.action = url;
        form.method = method;
        form.style = "display:none;";
        var ipt;
        if($.isArray(data)){// Array 对象 - 包含 - prototype新增的扩展对象 last/unset
            for(var k=0; k<data.length; k++){
                ipt = document.createElement("textarea");
                ipt.name = k;
                ipt.value = data[k];
                form.appendChild(ipt);
            }
        }
        else{
            for(var k in data){
                ipt = document.createElement("textarea");
                ipt.name = k;
                ipt.value = data[k];
                form.appendChild(ipt);
            }
        }
        document.body.appendChild(form);
        form.submit();
        return form;
    },
    post: function(url,data){return this.form(url,data,"post");},
    get: function(url,data){return this.form(url,data,"get");},
    // js/模拟保单	<-----------------------------------------------------------------------
    // 获取formJson - 选择器下元素的值
    formJson: function(selector){
        var el = this.is_object(selector)? selector:$(selector);
        if(el.length>0){
            var saveData = {};
            // input
            var ipts = el.find("input");
            var El,key,i=0;
            for(i=0; i<ipts.length; i++){
                El = $(ipts[i]);
                if(El.attr('type') == 'file') continue; // 文件控件不获取
                else if(El.attr('disabled')) continue; // 忽略禁用元素
                else if(El.attr('type') == 'checkbox' && !El.is(':checked')) continue;// 忽略未被选中的复选框
                else if(El.attr('type') == 'radio' && !El.is(':checked')) continue;// 忽略未被选中的单选框
                key = El.attr("name");
                if(this.empty(key)) continue;
                saveData[key] = El.val();
            }
            // textarea
            ipts = el.find('textarea');
            for(i=0; i<ipts.length; i++){
                El = $(ipts[i]);
                key = El.attr("name");
                if(this.empty(key)) continue;
                saveData[key] = El.val();
            }
            // select
            // wap 表单值获取报错，无此选择器 select option:selected zepto.js
            var sels = el.find("select");
            for(i=0; i<sels.length; i++){
                var tmpSelDoc = sels[i];
                El = $(tmpSelDoc);
                key = El.attr("name");
                if(key == "") continue;
                // bug 修复 - zepto.js
                try{
                    saveData[key] = El.find('option:selected').val();
                }catch(e){
                    var tmpSelectedIdx = tmpSelDoc.selectedIndex;
                    saveData[key] = tmpSelDoc.options[tmpSelectedIdx].value;
                }
            }
            return saveData;
        }
        return null;
    },
    /**
     * required 依然有效，即requird 要求的不符合要求
     * @constructor
     */
    IsRequired: function (selector) {
        var els = selector? $(selector).find('[required]'):$('[required]');
        for(var i=0; i<els.length; i++){
            var el = $(els[i]);
            if(this.formVal(el) == '') return true;
        }
        return false;
    },
    // 表单有效值
    formVal: function (selector) {
        var el = this.is_object(selector)? selector:$(selector);
        var value = '';
        if(el.is('input')) {
            if (el.attr('type') == 'checkbox' && !el.is(':checked')) value = '';// 忽略未被选中的复选框
            else if (el.attr('type') == 'radio' && !el.is(':checked')) value = '';// 忽略未被选中的单选框
            else value = el.val();
        }else if(el.is('select')){
            value = el.find('option:selected').val();
        }else
            value = el.val();
        return value;
    },
    /**
     * 后去保存数据, 通过 js__col 保存的, 键值 [name/col-name]
     * @param selector
     */
    getSaveData: function (selector) {
        var savedata = {};
        // 作用域
        var cols = selector? $(selector).find('.js__col'): $('.js__col');
        for(var i=0; i<cols.length; i++){
            var el = $(cols[i]);
            if(el.attr('disabled')) continue; // 忽略禁用元素
            var key = el.attr("name");
            key = key? key: el.attr('col-name');
            if(this.empty(key)) continue;
            if(el.is('input')) {
                if (el.attr('type') == 'checkbox' && !el.is(':checked')) continue;// 忽略未被选中的复选框
                if (el.attr('type') == 'radio' && !el.is(':checked')) continue;// 忽略未被选中的单选框
                savedata[key] = el.val();
            }else if(el.is('select')){
                var value = el.find('option:selected').val();
                savedata[key] = value;
            }else
                savedata[key] = el.val();
        }
        return savedata;
    }
}


export default Form