/**
 * 数据验证器
 * 2017年12月26日 星期二
 */

// 数据验证器
var Validate = {
    isEmail: function (value) {
        var reg = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
        return reg.test(value);
    },
    isPhone: function (value) {
        var reg = /^1[0-9]{10}$/;
        return reg.test(value);
    },
    isChinese: function (value) {
        var reg = /^[\u2E80-\u9FFF]+$/;
        return reg.test(value);
    }
}

export default Validate
