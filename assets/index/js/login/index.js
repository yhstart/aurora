/**
 * 2017年7月18日 星期二
 * 用户登录
 */
import {
    instance
} from '../../../src/Web'
// 系统全局性变量
window.Web = instance

$(function(){
    var $authLogin = new instance.autoLogin();
    $('[data-toggle="tooltip"]').tooltip();
    // 验证码刷新
    function refreshCode(dom) {
        dom = dom? dom:$('#recode_lnk');
        var xhtml = '<img class="weui-vcode-img" src="/captcha.jpg?image='+(Math.random()*100)+'">';
        dom.html(xhtml);
        //console.log(src,img);
    }
    
    /**
     * 登录记住
     * @param {Object} data
     * @param {Object} rdata 
     */
    function rememberRegister(data, rdata){
        if(rdata && rdata.remember && 'object' == typeof rdata.remember){
            var feek_token = (rdata && rdata.remember)? rdata.remember.feek_token || false : false;
            if(feek_token){
                $autoLogin.add({
                    rmbr_ftoken: feek_token,
                    account : data.account
                });
            }
        }
    }
    refreshCode();    
    $authLogin.loging(function(rdata){
        if(rdata.code == 1){
            location.href = '/center/bin';
        }
    });
    // 验证码更换
    $('#recode_lnk').click(function () {
        refreshCode($(this));
    });
    
    // 数据提交
    $('button[type="submit"]').click(function () {
        var data = Web.formJson('form');
        if('' != data.account && '' != data.pswd && '' != data.code){
            var loading = Web.loading();
            Web.ApiRequest('login/auth',data,function (rdata) {
                loading.hide();
                if(rdata.code == 1){
                    rememberRegister(data, rdata);
                    refreshCode();
                    Web.modal_alert(rdata.msg);
                    setTimeout(function () {
                        location.href = '/api/bin/center';
                    },3000);
                }
                else{
                    Web.modal_alert(rdata.msg);
                }
            });
        }
    });
});