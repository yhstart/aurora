/**
 * 2017年7月18日 星期二
 * 用户登录
 */
import {
    instance
} from '../../../src/Web'
// 系统全局性变量
window.Web = instance

$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    // 验证码刷新
    function reflushCode() {
        var img = $('#recode_lnk').find('img');
        var src = img.attr("src");
        img.attr('src',src);
    }
    // 验证码更换
    $('#recode_lnk').click(function () {
        reflushCode();
    });
    // 用户代码监听
    var accountListener = Web.FormAlerter('#account-ipter');
    var formInvalid = false;
    $('#account-ipter').blur(function () {
        var dom = $(this);
        accountListener.init();
        var account = dom.val();
        if(account){
            // 合法性检测
            var reg = /^[a-z0-9_-]{3,30}$/i;
            if(!account.match(reg)){
                var text = "【"+account+"】无效，账号不符合要求！";
                accountListener.danger(text);
                formInvalid = true;
                return;
            }
            Web.ApiRequest('register/check',{type:'account',value:account},function (rdata) {
                if(-1 == rdata.code){
                    accountListener.danger(rdata.msg);
                    formInvalid = true;
                }else{
                    accountListener.success("【"+account+"】有效");
                    formInvalid = false;
                }
            });
        }
    });
    // 密码监听
    var pswdCkListener = Web.FormAlerter('#pswdck-ipter');
    $('#pswdck-ipter').blur(function () {
        pswdCkListener.init();
        var pswd = $('#pswd-ipter').val();
        var pswdCk = $(this).val();
        if(pswdCk){
            if(pswdCk != pswd){
                formInvalid = true;
                pswdCkListener.danger('密码前后不一致！');
            }else if(pswdCk.length < 5){
                formInvalid = true;
                pswdCkListener.danger('密码长度必须大于等6位！');
            }
            formInvalid = false;
        }
    });
    // 注册表单提交
    $('#submit_btn').click(function () {
       if(!Web.IsRequired('form')){
           if(formInvalid){
               return Web.modal_alert('注册表单填写有误，就根据提示修改！');
           }
           var saveData = Web.formJson('form');
           if(saveData.pswd != saveData.pswdck || saveData.pswdck.length < 5){
               pswdCkListener.danger('密码前后不一致或者长度小于6位!');
               return;
           }
           var loading = Web.loading();
           Web.ApiRequest('register/save',saveData,function (rdata) {
               loading.hide();
               if(rdata.code == -1){
                   reflushCode();
                   Web.modal_alert(rdata.msg);
               }else{
                   setTimeout(function () {
                        location.href = Web._baseurl;
                   },3000);
                   Web.modal_alert('【'+ saveData.account+'】账号注册成功，且3秒后页面将跳转至首页!');
               }
           });
       }
    });
});