/**
 * Created by Administrator on 2017/5/11 0011.
 */

import {
    instance
} from '../../../src/Web'

// 系统全局性变量
if(!window.Web){
    window.Web = instance
}

$(function () {
    /**
     * 首页app
     * @class IndexApp
     */
    class IndexApp{
        constructor(){
            // 存储器
            this.storage = Web.storage().table('aurora_idx_idx');
            this.essayOrderCol = this.storage.select('essayOrderCol');
            // 文章自动排序字段
            if(this.essayOrderCol){
                $('#essay_order_selector')[0].value = this.essayOrderCol;
            }
        }
        /**
         * 页面初始化
         * @memberof IndexApp
         */
        pageInit(){
            var $instance = this;
            // dom 监听事件
            this.DomListener();
            // 获取文章列表
            this.getEssay();
            // 获取通知公告
            this.getSysInformation();

            // 图标绘制
            Web.ApiRequest('index/visit_count',null,function (rdata) {
                $instance.drawVchart(rdata);
            });
            Web.ApiRequest('visit/getDistributionCtt',null,function (rdata) {
                $instance.drawTiduChart(rdata);
            });

            // 自动登录
            if(!Web.getJsVar('user')){
                this.AutoLoaderHandler();
            }
            
        }
        /**
         * 获取文章列表
         * @param {any} orderBy 
         * @memberof IndexApp
         */
        getEssay(orderBy){
            orderBy = orderBy || this.essayOrderCol;
            var num = 8;
            Web.ApiRequest('essay/getList',{order: orderBy, num: num},function (rdata) {
                var dd = rdata.data;
                var html = '';
                for(var i=0; i<dd.length; i++){
                    var d = dd[i];
                    d.read_count = d.read_count || 1;
                    d.star_count = d.star_count? '<i class="fa fa-star text-success"></i>' + d.star_count: '';
                    html += `
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div class="mr-auto p-2">
                            <a href="${Web._baseurl}index/essay/read/item/${d.listid}.html" class="font-weight-bold text-info" target="_blank">${d.title}</a>
                            <span class="font-italic">${d.collected}</span>
                        </div>
                        <div class="p-2">
                            <i class="fa fa-eye text-danger"></i> ${d.read_count}
                            ${d.star_count}
                            ${d.date}
                        </div>
                        </li>
                    `;
                }
                $('#essay_list_ul').html(html);
            });
        }
        // 通知公告
        getSysInformation(){
            var num = 8;
            Web.ApiRequest('inform/getList',{num: num},function (rdata) {
                var dd = rdata.data;
                var html = '';
                for(var i=0; i<dd.length; i++){
                    var d = dd[i];
                    d.read_count = d.read_count || 1;
                    d.star_count = d.star_count? '<i class="fa fa-star text-success"></i>' + d.star_count: '';
                    html += `
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div class="mr-auto p-2">
                            <a href="${Web._baseurl}index/inform/read/item/${d.listid}.html" class="font-weight-bold text-info" target="_blank">${d.title}</a>
                        </div>
                        <div class="p-2">
                            <i class="fa fa-eye text-danger"></i> ${d.read_count}
                            ${d.star_count}
                            ${d.push_time}
                        </div>
                        </li>
                    `;
                }
                $('#sysnew_list_ul').html(html);
            });
        }
        /**
         * 手机与桌面访问对比
         * @param {any} rdata 
         * @memberof IndexApp
         */
        drawVchart(rdata){
             // echart 构图
            var vchart = echarts.init(document.getElementById('visit_chart'));
            var option = {
                animation: true,
                title: {
                    left: 'center',
                    text: '访问统计曲线图'
                },
                toolbox: {
                    itemSize: 25,
                    top: 55,
                    feature: {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        restore: {},
                        saveAsImage : {show: true}
                    }
                },
                tooltip:{
                    trigger: 'none',
                    axisPointer: {
                        type: 'cross'
                    }
                },
                yAxis: {
                    type: 'value',
                    axisTick: {
                        inside: true
                    },
                    splitLine: {
                        show: false
                    },
                    axisLabel: {
                        inside: true,
                        formatter: '{value}\n'
                    },
                    z: 10
                },
                xAxis: {
                    data: rdata.xAxis
                },
                series: [
                    {
                        name: '全部',
                        smooth: true,
                        symbol: 'circle',
                        symbolSize: 5,
                        sampling: 'average',
                        itemStyle: {
                            normal: {
                                color: '#8ec6ad'
                            }
                        },
                        type: 'line',
                        data: rdata.series[0]
                    },
                    {
                        name: '移动端',
                        smooth: true,
                        symbol: 'circle',
                        symbolSize: 5,
                        sampling: 'average',
                        itemStyle: {
                            normal: {
                                color: '#d68262'
                            }
                        },
                        type: 'line',
                        data: rdata.series[1]
                    }
                ]
            };
            vchart.setOption(option);
        }
        /**
         * 绘制地区访问配置图
         * @param {any} rdata 
         * @memberof IndexApp
         */
        drawTiduChart(rdata){
            $.get(Web._baseurl+'static/lib/echart/source/china.json', function (chinaJson) {
                echarts.registerMap('china', chinaJson);
                var chart = echarts.init(document.getElementById('visit_ditu_chrart'));
                chart.setOption({
                    title: {
                        left: 'center',
                        text: '访问地区分布图'
                    },
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data:['地区分布']
                    },
                    visualMap: {
                        min: 0,
                        max: 2500,
                        left: 'left',
                        top: 'bottom',
                        text: ['高','低'],           // 文本，默认为数值文本
                        calculable: true
                    },
                    toolbox: {
                        show: true,
                        orient: 'vertical',
                        left: 'right',
                        top: 'center',
                        feature: {
                            restore: {},
                            saveAsImage: {}
                        }
                    },
                    series: [{
                        name:'地区分布',
                        type: 'map',
                        map: 'china',
                        roam: false,
                        data:rdata
                    }]
                });
            });
        }
        /**
         * 事件监听
         * @memberof IndexApp
         */
        DomListener(){
            var $instance = this;
            $('.js__tooltip').tooltip();
            // toggle 显示
            $('.js__toggle').click(function () {
                var id = $(this).attr("data-id");
                $('.js__toggle_dc').addClass('d-none');
                $('#'+id).toggleClass('d-none');
            });      
            // 监听器 - 文章排序
            $('#essay_order_selector').change(function(){
                var value = this.value;
                if(value){
                    $instance.storage.update('essayOrderCol', value);
                }
                $instance.getEssay(value);
            });
        }
        /**
         * 系统自动登录
         */
        AutoLoaderHandler(){
            var $autoLogin = new Web.autoLogin();
            $autoLogin.loging(function(){});
        }
    }
    var $app = new IndexApp();
    $app.pageInit();
});