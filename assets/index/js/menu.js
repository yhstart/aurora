/**
 * 2017年11月24日 星期五
 * 菜单自适应
 */
$(function(){
    var think = Web.Think();
    /**
     * 菜单处理
     */
    class Menu{
        constructor(){
            this.autoAdjustMenuActive();
            this.addNewMenuByUser();
        }
        /**
         * 自动适应菜单为激活状态
         */
        autoAdjustMenuActive(){
            var clss = think.request('controller');
            var bar = $('#aurora_navbar [data-key="'+clss+'"]');
            if(bar.length > 0){
                bar.addClass('active');
            }
        }
        /**
         * 根据用户自动加载系统菜单
         */
        addNewMenuByUser(){
            Web.ApiRequest('aurora/menu', null, function(dd){
                if(dd.code == 1){
                    var list = dd.data;
                    var xhtml = '';
                    for(var i=0; i<list.length; i++){
                        var el = list[i];
                        xhtml += '<li class="nav-item"><a class="nav-link" href="'+el.url+'">'+el.text+'</a></li>';
                    }
                    if(xhtml){
                        $('#aurora_navbar ul').append(xhtml);
                    }
                }
            });
        }
    }
    new Menu();
});