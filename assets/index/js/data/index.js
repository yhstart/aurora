/**
 * 数据统计分析 js
 * 2017年12月30日 星期六
 */
import {
    instance
} from '../../../src/Web'
// 系统全局性变量
window.Web = instance
$(function(){
    // tab 显示标记
    var TabShowMk = {rank:{}, chart:{}},
        RgnTypeMap = {city: 'CT', province:'PV', country:'CN'},      // 地区分析字典
        RgnTypeQueue = {descrip: {city: '【城市】分布', province:'【省份/州】分布', country:'【国家/地区】分布'}},
        $aroPrg,
        CurYeay = (new Date()).getFullYear()

    class App{
        constructor(){
            this.curHash = instance.getHash()       // 当前页面hash值
            this.DomListener()
            this.ActiveTabNav()
            this.FirstLoadData()         
            $aroPrg = instance.AroProgresser()
        }
        // dom 监听器
        DomListener(){
            var $app = this
            // 地区分布 tab 监听
            $('#data-region ul.nav a.nav-link').click(function(){
                var dom = $(this)
                var item = dom.data('item')
                // 可切换插件
                if('toggle' == item){
                    var i = dom.find('i')
                    var cls = i.attr('class')
                    if(cls.indexOf('double-up') > -1){
                        i.attr('class', 'fa fa-angle-double-down')
                        $('#data-region > div.card-body').addClass('d-none')
                    }else{
                        i.attr('class', 'fa fa-angle-double-up')
                        $('#data-region > div.card-body').removeClass('d-none')
                    }
                    return null
                }
                $app.ActiveTabNav(item)
                if(!TabShowMk.rank[item]){                    
                    $app.RegionData(item)
                }
                if(!TabShowMk.chart[item]){
                    $app.RegionDataChart(item)
                }
            })
            // 地区分布年份改变时
            $('#data-region > div.card-body div.js__chart select[data-col="year"]').change(function(){
                var dom = $(this)
                var value = dom.find('option:selected').val()
                if(value){
                    $app.DataRegionMonth(value)
                }                
                // console.log(value)
                var item = $app.DgRgnActiveNav()
                if(item){
                    $app.RegionData(item)
                    $app.RegionDataChart(item)
                }
            })
            // 地区分布年份月份
            $('#data-region > div.card-body div.js__chart select[data-col="month"]').change(function(){
                var dom = $(this)
                var value = dom.find('option:selected').val()
                var item = $app.DgRgnActiveNav()
                if(item){
                    $app.RegionData(item)
                    $app.RegionDataChart(item)
                }              
            })
        }
        /**
         * 最初加载数据
         */
        FirstLoadData(){
            var $app = this
            // 地区分布数据加载
            var rgnDtType = this.curHash
            rgnDtType = rgnDtType? rgnDtType:'city'            
            if(rgnDtType){
                this.RegionData(rgnDtType)
            }
            // 年份加载
            instance.ApiRequest('visit/range', {type: 'year'}, function(rs){                
                var dd = rs.data
                //console.log(dd)
                var xOption = ''
                for(var i=0; i<dd.length; i++){
                    var row = dd[i]
                    if(CurYeay == row.value){
                        $app.DataRegionMonth(row.value, true)
                    }
                    xOption += '<option value="'+row.value+'">'+row.value+'</option>'
                }
                xOption += '<option value="">不限</option>'
                $('#data-region select[data-col="year"]').html(xOption)
                if(rgnDtType){
                    $app.RegionDataChart(rgnDtType)
                }
            })
        }
        /**
         * 获取区域份数当前激活nav名称
         * @returns {string}
         */
        DgRgnActiveNav(){
            var item = null
            var aEl = $('#data-region > div.card-header a.active')
            if(aEl.length > 0){
                item = aEl.data('item')
            }
            return item
        }
        /**
         * 访问数据清洗
         * @param {obejct} data 
         */
        // _vistDataClear(data){
        //     // 原始数据处理
        //     var dd = [], zhDic = {};
        //     for(var k=0; k<data.length; k++){
        //         var rawDt = data[k],
        //             name = rawDt.name 
        //         if(name == 'XX'){
        //             name = '其他';
        //         }else{
        //             name = name.replace(new RegExp("省"), '')
        //             name = name.replace(new RegExp("市"), '')
        //             if(zhDic[name]){
        //                 dd[zhDic[name][0]].value += zhDic[name][1]
        //                 continue
        //             }else{
        //                 zhDic[name] = [dd.length, rawDt.value]
        //             }
        //         }
        //         rawDt.name = name
        //         dd.push(rawDt);
        //     }
            
        //     // 数据排序

        //     return dd
        // }
        /**
         * 地区分布月区间
         * @param {number} year 
         * @param {boolean} allMk 
         */
        DataRegionMonth(year, allMk){
            var $app = this
            allMk = allMk? allMk:false
            if(year){
                instance.ApiRequest('visit/range', {type: 'month', year}, function(rs){
                    var dd = rs.data
                    var xOption = '<option value="">全年</option>'
                    for(var i=0; i<dd.length; i++){
                        var row = dd[i]
                        if(CurYeay == row.value){
                            $app.DataRegionMonth(row.value)
                        }
                        xOption += '<option value="'+row.value+'">'+row.value+'</option>'
                    }
                    // 全部
                    if(allMk){
                        $('#data-region select[data-col="month"]').html(xOption)
                    }else{
                        var item = $app.DgRgnActiveNav()
                        if(item){
                            $('#data-region > div.card-body > [data-item="'+item+'"] select[data-col="month"]').html(xOption)
                        }
                    }
                })
            }
        }
        /**
         * tab 激活方法
         * @param {string} name 为空时自动从hash中获取
         */
        ActiveTabNav(name){   
            name = name? name: this.curHash
            if(name){
                var nav = $('#data-region a[data-item="'+name+'"]')
                if(nav.length > 0){
                    $('#data-region ul.nav a.nav-link').removeClass('active')
                    nav.addClass('active')
                    // tab 同步
                    $('#data-region .card-body > .row').addClass('d-none')
                    $('#data-region .card-body').find('[data-item="'+name+'"]').removeClass('d-none')
                }
            }
        }
        /**
         * 地区数据分析
         * @param {string} type 请求类型
         */
        RegionData(type){
            var rType = type,    // 原始类型
                $this = this
            if(type){
                type = RgnTypeMap[type] || false
            }
            if(!type){
                return null
            }
            var page = 1
            var cardBody = $('#data-region > div.card-body > div[data-item="'+rType+'"]')
            var year = cardBody.find('select[data-col="year"]').find('option:selected').val()
            var month = cardBody.find('select[data-col="month"]').find('option:selected').val()
            cardBody.find('.js__rank').html('<div>数据记载中……</div>')
            instance.ApiRequest('visit/distributionctt', {type, page, year, month}, function(rs){
                TabShowMk.rank[rType] = true
                var xRank = ''
                // var dd = $this._vistDataClear(rs.data)
                var dd = rs.data
                // console.log(dd, zhDic)
                for(var i=0; i<dd.length; i++){
                    var row = dd[i]
                    xRank += '<li class="list-group-item d-flex justify-content-between align-items-center">'+row.name+'<span class="badge badge-pill badge-success">'+row.value+'</span></li>'
                }
                if(xRank){
                    xRank = '<ul class="list-group">'+xRank+'</ul>'
                }
                // 删除动态加载框
                $aroPrg.delByDom(cardBody.find('.js__rank .aro-progresser'))   
                if(!xRank){
                    xRank = '<div class="alert alert-danger" role="alert">数据加载失败(可能不存在数据)!</div>'
                }             
                cardBody.find('.js__rank').html(xRank)
            })
        }      
        /**
         * 地区数据分析（charts）
         * @param {string} type 请求类型
         */
        RegionDataChart(type){
            var rType = type    // 原始类型            
            if(type){
                type = RgnTypeMap[type] || false
            }
            if(!type){
                return null
            }            
            var chartEl = $('#data-region > div.card-body [data-item="'+rType+'"] div.js__chart')
            var year = chartEl.find('select[data-col="year"]').find('option:selected').val()
            var month = chartEl.find('select[data-col="month"]').find('option:selected').val()
            instance.ApiRequest('visit/distributionchart', {type, year, month}, function(rs){
                // 显示成功以后的标记
                TabShowMk.chart[rType] = true
                var data = rs.data
                data.text = {
                    "text": RgnTypeQueue.descrip[rType]
                }
                data.yAxis = {}
                data.tooltip = {}
                // console.log(data)
                var el = chartEl.find('div[data-id="charts"]')
                if(el.length > 0){
                    var chart = echarts.init(el.get(0))
                    // console.log(chart)
                    // 清空画布，防止缓存: 检测对象是否已经存在
                    chart.clear()
                    chart.setOption(data)
                }
                
            })
        }  
    }
    new App()    
})