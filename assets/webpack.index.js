/**
 * index 项目打包配置
 * 2017年12月26日 星期二
 */

const {
    AuroraJsPacker,
    nodeEnv
}  = require('./webpack.config')



var options = {
    baseDir: 'index/js/'
}
var FileQueue = (new AuroraJsPacker(options)).js([
    'index/index.js',                // 项目首页
    'essay/read.js',                 // 文章阅读
    'graffiti/index.js',             // 涂鸦首页
    'login/index.js',                // 登录页面首页
    'register/index.js',             // 注册页面首页
    'data/index.js',                // 数据统计分析首页
    'menu.js'                // 项目首页，测试
])
.getFiles()

//console.log(FileQueue)

module.exports = FileQueue