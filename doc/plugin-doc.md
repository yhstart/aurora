# 插件先关文档
- 2017年11月15日 星期三
- Joshua Conero


## textbox.io   - 用于移动端的富文本的插件
- 引入日期： 2017年11月15日 星期三
- [textbox.io](https://textbox.io/)
- 新建编辑器
```javascript
    // 参数/属性配置
    var config = {};
    var editor = textboxio.replace('.editableDiv', config);
    // Create a Textbox.io Editor for the matched element(s)
    var divEditor = textboxio.replace('#editableDiv');
    var textareaEditor = textboxio.replace('#mytextarea');

    // 移除编辑器
    // Remove Textbox.io Editor and update the div contents
    divEditor.restore();



    // Get the Active Editor Instance
    // Create a Textbox.io Editor for the matched element(s)
    textboxio.replaceAll('.editableDiv');

    document.getElementById('getActiveContent').onclick = function() {
        // Get the active editor
        var editor = textboxio.getActiveEditor();
        
        // Retrieve the contents of the active editor
        var content = editor.content.get();
        alert(content);
    };


    // 设置富文本参数值
    // Create a Textbox.io Editor for the matched element
    var divEditor = textboxio.replace('#editableDiv');
    var newContent = '<h1>Content set with editor.content.set()</h1>';
    document.getElementById('setContentButton').onclick = function() {
        // Set editor content
        divEditor.content.set(newContent);
    };

    // 获取富文本参数是
    // Create a Textbox.io Editor for the matched element
    var divEditor = textboxio.replace('#editableDiv');
    document.getElementById('getContentButton').onclick = function() {
        // Get & Alert editor content
        var content = divEditor.content.get();
        alert(content);
    };


```
