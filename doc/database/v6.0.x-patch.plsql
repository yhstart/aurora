/*drop procedure uname_procedure;
delimiter $$;
create procedure uname_procedure()
begin
    select 1*8;
    select * from information_schema.columns where table_schema='jc_aurora' and table_name='sys_login' and column_name='ip';
end;

-- 执行命令行
call uname_procedure();
*/

-- IP 地址边长，由于IP获取错误的问题
alter table `sys_login` modify `ip` varchar(50);
# select * from information_schema.columns where table_schema='jc_aurora' and table_name='sys_login' and column_name='ip';