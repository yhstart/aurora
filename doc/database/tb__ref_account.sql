-- @2019年8月8日 星期四
-- @Joshua Conero
-- @参照对账表, 微信、支付宝
-- @数据导入，可以使用数据库管理工具如 DBeaver


-- 参照对账表
create table if not exists ref_account(
    `id` int unsigned primary key auto_increment not null comment '主键',
    `account_name` varchar(40) comment '账号名称',
    `user` varchar(40) comment '用户',
    `mtime` datetime not null default CURRENT_TIMESTAMP comment '记录创建时间',
    -- 参照支付宝账单
    `deal_no` varchar(100) comment '交易号',
    `order_no` varchar(100) comment '订单号',
    `create_time` datetime comment '交易创建时间',
    `pay_time` datetime comment '付款时间',
    `modify_time` datetime comment '最近修改时间',
    `deal_src`  varchar(100) comment '交易来源地',
    `deal_type` varchar(80) comment '类型',
    `deal_party` varchar(80) comment '交易对方',
    `goods` varchar(120) comment '商品名称',
    `money` decimal(10, 2) comment '金额（元）',
    `income` varchar(10) comment '收/支',
    `status` varchar(50) comment '交易状态',
    `service_charge` decimal(10, 2) comment '服务费（元）',
    `refund` decimal(8, 2) comment '成功退款（元）',
    `remark` varchar(100) comment '备注',
    `money_status` varchar(20) comment '资金状态',
    -- 微信附加信息
    `pay_method` varchar(40) comment '支付方式'
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8
;


-- 支付宝导入表格式
CREATE TABLE `imp_alipay_xyz` (
  `deal_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '交易号',
  `order_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '订单号',
  `create_time` datetime DEFAULT NULL COMMENT '交易创建时间',
  `pay_time` datetime DEFAULT NULL COMMENT '付款时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最近修改时间',
  `deal_src` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '交易来源地',
  `deal_type` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `deal_party` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '交易对方',
  `goods` varchar(120) COLLATE utf8_bin DEFAULT NULL COMMENT '商品名称',
  `money` decimal(10,2) DEFAULT NULL COMMENT '金额（元）',
  `income` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '收/支',
  `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '交易状态',
  `service_charge` decimal(10,2) DEFAULT NULL COMMENT '服务费（元）',
  `refund` decimal(8,2) DEFAULT NULL COMMENT '成功退款（元）',
  `remark` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  `money_status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '资金状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;
-- 导表语句
insert into ref_account(
  `account_name`, `user`, `deal_no`, `order_no`, `create_time`,
  `pay_time`, `modify_time`, `deal_src`, `deal_type`, `deal_party`,
  `goods`, `money`, `income`, `status`, `service_charge`, `refund`, `remark`, `money_status` 
) select
'alipay', 'conero',  trim(sc.`deal_no`), trim(sc.`order_no`), sc.`create_time`,
  sc.`pay_time`, sc.`modify_time`, trim(sc.`deal_src`), trim(sc.`deal_type`), trim(sc.`deal_party`),
  trim(sc.`goods`), sc.`money`, trim(sc.`income`), trim(sc.`status`), sc.`service_charge`, sc.`refund`, 
  trim(sc.`remark`), trim(sc.`money_status`)
from imp_alipay_190830 sc where not exists(
	select 1 from `ref_account` 
		where `account_name`='alipay' and `user`='conero' and `deal_no`=sc.`deal_no`
)
;



-- 微信导入表格式
CREATE TABLE `imp_wepay_xyz` (
  `create_time` datetime DEFAULT NULL COMMENT '交易创建时间',
  `deal_type` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `deal_party` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '交易对方',
  `goods` varchar(120) COLLATE utf8_bin DEFAULT NULL COMMENT '商品名称',
  `income` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '收/支',
  `money` decimal(10,2) DEFAULT NULL COMMENT '金额（元）',
  `pay_method` varchar(40) COLLATE utf8_bin DEFAULT NULL COMMENT '支付方式',
  `status` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '交易状态',
  `deal_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '交易号',
  `order_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '订单号',
  `remark` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;
-- 导表语句
insert into ref_account(
  `account_name`, `user`, `create_time`, `deal_type`, `deal_party`, `goods`,
`income`, `money`, `pay_method`, `status`, `deal_no`, `order_no`,
`remark` 
) select
    'wepay', 'conero',  `create_time`, trim(`deal_type`), trim(`deal_party`), trim(`goods`),
    trim(`income`), `money`, trim(`pay_method`), trim(`status`), trim(`deal_no`), trim(`order_no`),
    trim(`remark`)
from imp_wepay_190807 sc where not exists(
	select 1 from `ref_account` 
		where `account_name`='wepay' and `user`='conero' and `deal_no`=sc.`deal_no`
)
;