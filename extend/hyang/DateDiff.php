<?php
/**
 * 时间相加减扩展: $ => 1-2
 * User: Joshua Conero
 * Date: 2019/8/22
 * Time: 23:13
 * Email: conero@163.com
 */

namespace hyang;

/**
 * Class DateDiff
 * @package hyang
 * // get 继承属性，累计变量
 * @property $year
 * @property $month
 * @property $day   累计天数
 * @property $hour
 * @property $minute
 * @property $second
 */
class DateDiff
{
    const ItemYear = 'year';
    const ItemMonth = 'month';
    const ItemDay   = 'day';
    const ItemHour  = 'hour';
    const ItemMinute = 'minute';
    const ItemSecond = 'second';

    //=> date1 - date2
    protected $_date1;
    protected $_date2;

    /**
     * DateDiff constructor.
     * @param string|integer|\DateTime $date1
     * @throws \Exception
     */
    function __construct($date1)
    {
        $dataIns = $this->dateInstance($date1);
        if($dataIns){
            $this->_date1 = $dataIns;
        }
    }

    /**
     * @param string|integer|\DateTime $date2
     * @return \DateTime|null
     * @throws \Exception
     */
    function tdate($date2){
        $date2 = $this->dateInstance($date2);
        if($date2){
            $this->_date2 = $date2;
        }
        return $date2;
    }

    /**
     * @return bool|\DateInterval|null
     * @throws \Exception
     */
    private function _getDiff(){
        $date1 = $this->_date1;
        $diff = null;
        if($date1){
            $date2 = $this->_date2;
            $date2 = $date2? $date2: (new \DateTime('now'));
            $diff = $date1->diff($date2);
        }
        return $diff;
    }

    /**
     * @param \DateTime|string|integer $date
     * @return \DateTime|null
     * @throws \Exception
     */
    protected function dateInstance($date){
        $dataIns = null;
        if(is_int($date)){
            $dataIns = \DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s', $date));
        }
        elseif(is_object($date)){
            $dataIns = $date;
        }elseif (is_string($date)){
            $dataIns = new \DateTime($date);
        }
        return $dataIns;
    }

    /**
     * 刷新值
     * @return $this
     */
    function refresh(){
        $this->_date2 = null;
        return $this;
    }

    /**
     * @param string $name
     * @return null
     * @throws \Exception
     */
    function getItem($name){
        $value = null;
        $diff = $this->_getDiff();
        if($diff){
            $y = $diff->y;
            $m = $diff->m;
            $d = $diff->d;
            $h = $diff->h;
            $i = $diff->i;
            $s = $diff->s;
            // 默认获取日期
            $name = $name ?? self::ItemDay;
            switch ($name){
                case self::ItemYear:
                    // 忽略一年中 60分钟一集秒所占的比例；0.00010958
                    $value = $y + ($m/12) + ($d/365) + ($h/(365*24));
                    break;
                case self::ItemMonth:
                    // 忽略 60秒钟制度的影响
                    $value = $y*12 + $m + ($d/30) + ($h/(30*24)) + ($i/(30*24*60));
                    break;
                case self::ItemDay:
                    $value = $y*365 + $m*30 + $d + ($h/24) + ($i/(24*60)) + ($s/(24*60*60));
                    break;
                case self::ItemHour:
                    $value = $y*365*24 + $m*30*24 + $d*24 + $h + ($i/60) + ($s/(60*60));
                    break;
                case self::ItemMinute:
                    $value = $y*365*24*60 + $m*30*24*60 + $d*24*60 + $h*60 + $i + ($s/60);
                    break;
                case self::ItemSecond:
                    $value = $y*365*24*60*60 + $m*30*24*60*60 + $d*24*60*60 + $h*60*60 + $i*60 + $s;
                    break;
            }
        }
        return $value;
    }

    /**
     * @param $name
     * @return null
     */
    function __get($name)
    {
        try{
            $value = $this->getItem($name);
        }catch (\Exception $e){
            $value = null;
        }
        return $value;
    }
}