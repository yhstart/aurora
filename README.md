# [aurora_个人系统管理](http://www.conero.cn/)
- 2017年5月5日 星期五
- Doeeking V3
- Joshua Conero
- [版本说明](./suroong.md)



> base-tp5.1.x 基于tp5.1.x的版本（代号 ***suroong*** ）
>> 
        2018年1月2日 星期二
        准备将核心框架迁移至 tp5.1.x 版本 升级



**conero.cn/aurora **

> aurora PC版

<a href="http://aurora.conero.cn/">![aurora PC](https://aurora.conero.cn/aurora.png)</a>

> aurora 移动端

<a href="http://aurora.conero.cn/">![aurora 微信端](https://aurora.conero.cn/aurora-appm.png)</a>



## 安装

### 依赖

- AppServ8.6.0
        - php 5.6.30+
- redis 3.2.x
- ThinkPHP5.1.x
    - ThinkPHP5.0.9   2017年5月22日 星期一切换核心程序版本
    - [phpredis](https://github.com/phpredis/phpredis)
- composer $
    - composer require predis/predis        (2017年7月20日 星期四)



<br />  [移动端界面]

- zepto.js  v1.2.0            [网站](http://zeptojs.com/)
- Weui v2.0.0   移动端UI库     [文档](https://github.com/weui/weui/wiki)
- Weui.js v1.1.4  js  



### 初始化

- *`config/database.example.php` 更名为 `config/database.php`*



> MySQL

*配置信息*

```mysql
-- 删除数据库中 sql_mode = ONLY_FULL_GROUP_BY
show variables like '%sql_mode%';
-- 查询结果如下：
ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```







## 系统开发规定或说明
- alpha 版本可以不发布到实际网站，到需要提交代码版本控制器       @180128
- 版本计划中
    - 利用 TODO 标记，并对其进行编号
        - FB    bug 待修复






## 框架改造说明
- [aurora/center](https://git.oschina.net/Doee/aurora_center)
- 分区移动端与桌面端 appm/appd
- 公共部分 api

> *.sql 用于私有sql文件， plsql 规定为空发布sql文件





## 系统版本
- v0.4.x (20170712-2017xx/v0.4.0-v0.4.12)
        日志系统的实现(center 子项目建设)
        以及向前版本优化

- v0.3.x (20170709-20171108/v0.3.0-v0.3.15)

        个人中心实现(center 子项目建设)
        前向版本程序优化以及bug优化

- v0.2.x (20170611-20170708/v0.2.0-v0.2.16)

        wap模块建设： 财务系统/设计与实现
        web/系统管理后台 程序优化

- v0.1.x (20170514-20170610/v0.1.0-v0.1.23)

        建设 web/系统管理后台
        wap 开发页面优化以及设计

- v0.0.x (20170505-20170513/v0.0.0-v0.0.11)

        系统架构：ThinkPHP5.0.x + bootstrap4； 设计wap/web两个环境UI
        (浏览器)wap 主界面，前端模板设计
        (移动端)web 主界面，前端模板设计
        wap/web开放页面建设



## 微信公众号 Conero

![关注我们的公众号](http://aurora.conero.cn/static/img/conero-dyh-min.jpg)

