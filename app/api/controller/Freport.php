<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/10/22 0022 10:49
 * Email: brximl@163.com
 * Name: 财务报表
 */

namespace app\api\controller;


use app\common\controller\Api;
use think\Db;

class Freport extends Api
{
    //@ 后去可实现的财务账单列表
    public function index(){
        $uid = getUserInfo('uid');
        if($uid){
            $page = request()->param('page', 1);
            $num = request()->param('num', 15);
            $getYearDt = Db::table('fnc1000c')
                ->field(['date_format(date,\'%Y\') as year', 'count(*) as num', 'sum(money) as money'])
                ->where('uid', $uid)
                ->group('date_format(date,\'%Y\')')
                ->order('date desc')
                ->page($page, $num)
                ->select();
            $data = [];
            foreach ($getYearDt as $v){
                $year = $v['year'];
                $getMonthDt = Db::table('fnc1000c')
                    ->field(['date_format(date,\'%Y%m\') as month', 'count(*) as num', 'sum(money) as money'])
                    ->where('uid', $uid)
                    ->where('date_format(date,\'%Y\')=\''.$year.'\'')
                    ->group('date_format(date,\'%Y%m\')')
                    ->order('date desc')
                    ->select();
                $month = [];
                foreach ($getMonthDt as $vv){
                    $month[$vv['month']] = [
                         'count' => $vv['num'],
                        'money' => $vv['money']
                    ];
                }
                $data[$year.'y'] = [
                    'month' => $month,
                    'money' => $v['money'],
                    'count' =>$v['num']
                ];
            }
            return $this->FeekMsg($data);
        }
        else{
            return $this->FeekMsg('您还没有登录系统，数据请求已被屏蔽');
        }
    }
    //@ 通过财务账单自动生成表表
    public function dynamic(){
        $uid = getUserInfo('uid');
        if($uid){
            $type = request()->param('type');
            $value = request()->param('value');
            if($type && $value){
                $data = Db::query('call finance_creport_sp(?, ?)', [$uid, $value]);
                $data = isset($data[0])? (isset($data[0][0])? $data[0][0]: array()) : array();
                return $this->FeekMsg($data);
            }
            else{
                return $this->FeekMsg('请求参数不足， {type: 类型, value: 值}');
            }
        }
        else{
            return $this->FeekMsg('您还没有登录系统，数据请求已被屏蔽');
        }
    }
}