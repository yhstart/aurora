<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/10/2 0002 0:37
 * Email: brximl@163.com
 * Name: 系统消息通知
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Prj1002c;
use think\facade\Config;

class Inform extends Api
{
    // 获取系统消息
    public function getList(){
        $request = $this->request;
        $num = $request->param('num', 20);
        $prj2 = new Prj1002c();
        $data = $prj2->getInfos(Config::get('setting.prj_code'),false,function ($query) use($num){
            return $query->limit($num)->select();
        });
        if($data){
            $data = $data->toArray();
        }
        return $this->FeekMsg($data);
    }
}