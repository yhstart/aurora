<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 13:24
 * Email: brximl@163.com
 * Name: 日志系统 API
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\LgRecord;
use app\common\model\LgProject;

class Log extends Api
{
    // 搜索日志记录
    public function search(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $lg = new LgProject();
        $clg = new LgRecord();
        $ss = isset($_POST['ss'])? $_POST['ss']: null;
        $field = $this->request->param('field', 'a.*');    // 字段
        $page = $this->request->param('page', 1);
        $num = $this->request->param('num', 20);
        $where = ['uid'=> $uid];
        if($ss && is_array($ss)){
            $where = array_merge($where, $ss);
        }
        $subQuery = $clg
            ->field('count(*)')
            ->where('p_listid=a.listid')
            ->select(false)
            ;
        $rs = $lg
            ->alias('a')
            ->field($field)
            ->field(["($subQuery)"=>'count'])
            ->where($where)
            ->page($page, $num)
            ->order('mtime desc')
            ->select()
        ;
        return $this->FeekMsg($rs);
    }
}