<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/10/29 0029 23:54
 * Email: brximl@163.com
 * Name:
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Fnc0030c;

class Fsubject extends Api
{
    // 获取数据列表
    public function sets(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $page = request()->param('page', 1);
        $num = request()->param('num', 20);
        $fnc = new Fnc0030c();
        $data = $fnc
            ->alias('a')
            ->join('sys_user b','a.uid=b.uid')
            ->field(['a.listid','a.subject','a.private_mk','a.uid','a.mtime','date_format(`a`.`mtime`,\'%Y-%m-%d\')'=>'date','b.account as author'])
            ->where(['a.private_mk'=>'Y','a.uid'=>$uid])
            ->whereOr(['a.private_mk'=>'N'])
            ->page($page,$num)
            ->order('a.private_mk,a.mtime desc')
            ->select();
        $count = $fnc
            ->alias('a')
            ->join('sys_user b','a.uid=b.uid')
            ->where(['a.private_mk'=>'Y','a.uid'=>$uid])
            ->whereOr(['a.private_mk'=>'N'])
            ->count();

        return $this->FeekMsg([
            'result' => $data,
            'page' => $page,
            'count'=> $count
        ]);
    }
}