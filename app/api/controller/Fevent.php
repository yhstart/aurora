<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/24 0024 21:01
 * Email: brximl@163.com
 * Name:
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Fnc2001c;
use think\Db;

class Fevent extends Api
{
    // 数据保存
    public function save(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        list($data,$mode,$map) = $this->_getSaveData();
        $fnc = new Fnc2001c();
        Db::startTrans();
        try{
            if($mode == 'A') {
                $data['uid'] = $uid;
                if($fnc->save($data)){
                    Db::commit();
                    return $this->FeekMsg('数据新增成功!',1);
                }
                return $this->FeekMsg('数据新增失败了！');
            }elseif ($mode == 'M'){
                if(!empty($map) && $fnc->save($data,$map)){
                    Db::commit();
                    return $this->FeekMsg('数据更新成功!',1);
                }
                return $this->FeekMsg('数据更新失败了！');
            }elseif ($mode == 'D'){
                if($this->pushRptBack($fnc->getTable(),$map,'auto')){
                    Db::commit();
                    return $this->FeekMsg('数据删除成功!',1);
                }
                return $this->FeekMsg('数据删除失败了！');
            }
        }catch (\Exception $e){
            debugOut($e->getMessage()."\r\n".$e->getTraceAsString());
            Db::rollback();
            return $this->FeekMsg('系统出错!');
        }
        //debugOut($data);
        return $this->FeekMsg('系统请求无效!');
    }

    /**
     * 用于获取当前用户（picker数据-weui）
     */
    public function picker(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $fnc = new Fnc2001c();
        $data = $fnc->where('uid',$uid)
            ->field('listid,title')
            ->select();
        $retVal = [];
        foreach ($data as $v){
            $retVal[] = [
                'label' => $v['title'],
                'value' => $v['listid']
            ];
        }
        if(count($retVal) > 0) return $this->FeekMsg($retVal);
        return $this->FeekMsg('您还没有财务纪事数据');
    }
    // 获取数据列表
    public function sets(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $page = request()->param('page');
        $num = request()->param('num');
        $page = is_numeric($page)? intval($page): 1;
        $num = is_numeric($num)? intval($num): 20;

        $fnc = new Fnc2001c();
        $data = $fnc
            ->alias('a')
            ->field(['a.title','a.mtime','a.descrip','a.listid',
                '(select count(*) from fnc2000c where pid=a.listid)'=>'plan_count'])
            ->where('a.uid',$uid)
            ->page($page, $num)
            ->select()
        ;
        $count = $fnc
            ->alias('a')
            ->where('a.uid',$uid)
            ->count()
        ;
        return $this->FeekMsg([
            'result' => $data,
            'page' => $page,
            'count'=> $count
        ]);
    }
}