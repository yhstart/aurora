<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/4/21 0021 17:22
 * Email: brximl@163.com
 * Name: 系统命令处理器
 */

namespace app\api\controller;


use hyang\Net;

class Bin
{
    // 跳转个人中心中间
    public function center(){
        echo '页面跳转中……';
        $token = getUserInfo('token');
        //println($token);
        if(!$token){
            echo  '<br>跳转参数无效，您可能没有登录系统！或者重新登录系统后在跳转……';
        }else{
            $env = config('env.'.env('APP_ENV'));
            Net::go($env['center_url'].'?token='.$token);
        }
    }
}