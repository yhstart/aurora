<?php
/**
 * Created by PhpStorm.
 * User: Joshua Conero
 * Date: 2019/8/14
 * Time: 23:44
 * Email: conero@163.com
 */

namespace app\api\controller;


use app\common\utils\EnumBase;
use app\common\utils\Mail;
use app\common\controller\Api;
use app\common\Aurora;
use app\common\model\User;

// 服务端，发送邮件超时
// 不限制时间
set_time_limit(0);

class Verify extends Api
{
    /**
     * 发送验证码
     * @return \think\response\Json
     * @throws \PHPMailer\PHPMailer\Exception
     */
    function code(){
        $email = input('param.email');
        if($email){
            Mail::sendCode($email);
            return $this->FeekMsg('邮件已发送', EnumBase::FeekSuccessCode);
        }
        return $this->FeekMsg('请求参数有误！');
    }

    /**
     * 邮箱认知
     * @return \think\response\Json
     */
    function check(){
        $email = input('param.email');
        $verify_code = input('param.verify_code');
        if($email && $verify_code){
            if(Mail::verifyCode($verify_code, $email)){
                $uid = getUserInfo('uid');
                if(empty($uid)){
                    Aurora::loginExit();
                    return $this->FeekMsg('您的登录信息有误，邮箱验证失败！');
                }
                $user = new User();
                // 更新数据
                $user->save([
                    'email' => $email,
                    'status'=> User::StatusCodeValid,
                    'last_lgtime' => date('Y-m-d H:i:s')
                ], ['uid' => $uid]);
                return $this->FeekMsg('邮箱正确，且已更新您的账号邮箱', EnumBase::FeekSuccessCode);
            }
            return $this->FeekMsg('您的验证码无效，请重新输入！');
        }
        return $this->FeekMsg('您的请求参数有误');
    }
}