<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 12:00
 * Email: brximl@163.com
 * Name:
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\LgProject as LgProjectModel;

class Lgproject extends Api
{
    // 数据保存
    public function save(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        list($data,$mode,$map) = $this->_getSaveData('listid');
        //debugOut([$data,$mode,$map]);
        $logP = new LgProjectModel();
        switch ($mode){
            case 'A':
                $data['uid'] = $uid;
                if($logP->save($data)){
                    return $this->FeekMsg('日志新增成功', 1);
                }
                return $this->FeekMsg('日志新增成功失败');
                break;
            case 'M':
                $map['uid'] = $uid;
                if($logP->save($data,$map)) return $this->FeekMsg('日志更新成功！',1);
                return $this->FeekMsg('日志更新失败！');
                break;
            case 'D':
                $map['uid'] = $uid;
                if($this->pushRptBack($logP->getTable(),$map,'auto'))
                    return $this->FeekMsg('日志删除成功！',1);
                return $this->FeekMsg('日志删除失败！');
                break;
        }
        return $this->FeekMsg('请求参数无效');
    }
}