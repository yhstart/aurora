<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/5/9 0009 22:25
 * Email: brximl@163.com
 * Name: 用户登录
 */

namespace app\api\controller;
use app\common\Aurora;
use app\common\model\Token;
use app\common\service\LoginSessionChecker;
use hyang\Net;
use hyang\HRedis;
use think\facade\Session;
use think\facade\Config;
use app\common\model\User;
use app\common\model\Prj1001c;
use app\common\controller\Api;
// 获取 token 时登录超时处理， nigix 环境 2018年6月22日 星期五
\set_time_limit(0);

class Login extends Api
{
    // 用户登录认证
    public function auth(){
        // 登录状态监测
        $user = getUserInfo('user');
        if($user){
            return $this->FeekMsg('您已经登录系统了', 1);
        }
        // 登录检测
        if(LoginSessionChecker::isLock()){
            return $this->FeekMsg('您登录次数尝试过多，已被锁定['.LoginSessionChecker::getLockMinute().']分钟！');
        }

        // 验证码自动验证 account/pswd/code[/remember]
        $account = trim(request()->param('account'));
        $pswd = trim(request()->param('pswd'));
        $code = trim(request()->param('code'));
        $rememberMk = trim(request()->param('remember'));
        $msg = '';
        $userModel = new User();
        if(!captcha_check($code)) $msg = '验证码无效';
        else{
            if(!$userModel->AccountExist($account)) $msg = '账户不存在！';
            else{
                if(!Aurora::checkUserPassw(
                    $pswd,
                    $userModel->getPassword(),
                    $userModel->where('uid',$userModel->uid)->value('salt')
                )) $msg = '密码不正确！';
            }
        }
        if(empty($msg)){
            $msg = Aurora::userLoginHandler($userModel->uid);
            if(empty($msg) && 'Y' == $rememberMk){
                $tokenMd = new Token();
                $rememberMk = $tokenMd->getAutoLoginToken($userModel->uid);
            }
        }
        $successData = ['code'=>1,'msg'=>''];
        // 返回记住令牌号
        if($rememberMk && 'Y' != $rememberMk){
            $successData['remember'] = ['feek_token'=>$rememberMk];
        }
        // 错误时记录尝试值
        if($msg){
            LoginSessionChecker::record();
        }
        return $msg? ['code'=>-1,'msg'=>$msg]:$successData;
    }
    /**
     * 开发者首页统计过滤
     * 参数： token * , url 自动跳转
     */
    public function developer(){
        // 令牌
        $token = request()->param('token');
        $url = request()->param('url');
        $msg = '';
        $badMsg = '开发者登入网站时，令牌无效！如果无令牌请向网站申请，且该权限只向开发者开放！';
        if($token){
            $isValid = (new Token())
                ->TokenIsValid($token);
            if($isValid){
                $this->autoRecordVisitRecord(false);
                // 跳转到首页
                if($url && 'home'== $url){
                    $this->redirect(IS_MOBILE == 'Y'? '/wap':'/index');
                }
                elseif($url) $this->redirect($url);
                else $this->redirect($this->getRootUrl());
            }
            else $msg = $badMsg;
        }
        else $msg = $badMsg;
        if($url){
            $this->getErrorUrl($msg);
        }
        return json([
            'code'=>($msg? -1: 1),
            'msg' => $msg? $msg:'认证成功！'
        ]);
    }

    /**
     * 系统注销
     */
    public function quit(){
        Aurora::loginExit();
        $this->getRootUrl(false);
    }
    /**
     * 获取登录状态,(统一ip下)用于不同项目登录用户获取
     * request: name,key
     */
     public function state(){
         $name = request()->param('name');
         $key = request()->param('key');
         $redirect = request()->param('redirect');      
        //  debugOut($redirect);   
         if($name && $key){
            $prj = new Prj1001c(); 
            $value = $prj->getSetVal($name,Config::get('setting.PROJECT_NAME'),true);
            if($key == $value){
                $data = getUserInfo();  
                // debugOut(Net::setQuery($redirect,['result' => bsjson($data)]));
                // echo $redirect."<br>".Net::setQuery($redirect,['result' => bsjson($data)]);die;
                if($redirect) go(Net::setQuery($redirect,['result' => bsjson($data)]));                                   
                return $this->FeekMsg($data);
            }
            // debugOut(Net::setQuery($redirect,['result' => bsjson($this->FeekMsg('请求参数无效！'))],true));  
            if($redirect) go(Net::setQuery($redirect,['result' => bsjson($this->FeekMsg('请求参数无效！'))],true)); 
            return $this->FeekMsg('请求参数无效！');
         }
        //  debugOut(Net::setQuery($redirect,['result' => bsjson($this->FeekMsg('缺少请求参数！'))],true));  
         if($redirect) go(Net::setQuery($redirect,['result' => bsjson($this->FeekMsg('缺少请求参数！'))],true));
         return $this->FeekMsg('缺少请求参数！');
     }

    /**
     * 自动登录，根据客服端存在的有效token值 / account
     */
     public function auto_auth(){
         $token = $this->request->param('token');
         $account = $this->request->param('account');
         if($token){
             // 登录状态监测
             $user = getUserInfo('user');
             if($user){
                 return $this->FeekMsg('您已经登录系统了', 1);
             }
             $tokenModel = new Token();
             $loginConf = config('setting.login');
             $map = [
                 'token' => $token,
                 'invalid_mk' => 'N',
                 'type'        => $loginConf['code']
             ];
             if($account){
                 $userModel = new User();
                 if(!$userModel->AccountExist($account)){
                     return $this->FeekMsg('自动登录失败!(用户代码不存在)');
                 }
                 $map['uid'] = $userModel->uid;
             }
             $rs = $tokenModel
                 ->where($map)
                 ->order('mtime desc')
                 ->find()
                 ;
             if($rs){
                 $mtime = $rs['mtime'];
                 $d1 = new \DateTime($mtime);
                 $interval = $d1->diff(new \DateTime('now'));
                 $day = $interval->format('%R%a');
                 $rs['day'] = intval($day);
                 if($rs['day'] > $loginConf['remember_day']){
                     // 过期以后是所有有效的代码失效
                     $tokenModel->save([
                         'invalid_mk' => 'Y'
                     ], ['uid' => $rs['uid'], 'type'=>'30']);
                     return $this->FeekMsg('请求参数已失效！');
                 }
                 // 自动登录系统
                 Aurora::userLoginHandler($rs['uid']);
                 return $this->FeekMsg($rs, 1);
             }
             return $this->FeekMsg('请求参数不存在或者已过期！');
         }
         return $this->FeekMsg('请求参数无效！');
     }

    /**
     * conero.cn 系单点登录(参数： token)
     */
     public function token(){
         $token = $this->request->param('token');
         if($token){
             // 登录状态监测
             $user = getUserInfo('user');
             if($user){
                 Net::go('/');
                 //return $this->FeekMsg('您已经登录系统了', 1);
             }
             // 接口处理
             $pigeons = $this->getPigeons();
             $header = [
                 'Conero-Code' => $pigeons->code,
                 'Conero-Pid'  => $pigeons->conero_pid
             ];
             // 获取用户信息
             $options = [
                 'data' => [
                     'token' => $token
                 ],
                 'header' => $header
             ];
             $userInfo = $pigeons->curl('aurora/user/info', $options);
             if(empty($userInfo) || !is_array($userInfo) || !isset($userInfo['code']) || 400 == intval($userInfo['code'])){
                 return $this->FeekMsg('token值无效，数据操作失败！');
             }
             // 自动登录系统
             Aurora::userLoginHandler($userInfo['uid'], [
                 'token' => $token
             ]);
             Net::go('/');
         }
         return $this->FeekMsg('参数无效！');
     }
}