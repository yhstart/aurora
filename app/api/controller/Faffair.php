<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/15 0015 23:18
 * Email: brximl@163.com
 * Name: 事务甲乙方API
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Fnc0020c;
use app\common\model\Fnc1000c;
use app\common\SvFaffair;
use hyang\Util;

class Faffair extends Api
{
    // 数据保存
    public function save(){
        $uid = getUserInfo('uid');
        // 用户未登录时
        if(empty($uid)) return $this->FeekMsg('您的凭证无效!');
        list($data,$mode,$map) = $this->_getSaveData();
        $data = Util::dataClear($data,['start_use']);
        if($mode == 'A'){
            $data['uid'] = $uid;
            if(!empty($data['command'])){
                $data = array_merge($data, SvFaffair::getCmd($data['command']));
            }
            $fnc20c = new Fnc0020c($data);
            if($fnc20c->where(['uid'=>$uid,'name'=>$data['name'],'type'=>$data['type']])->count()){
                return $this->FeekMsg('【'.$data['name'].'】已经存在!');
            }
            if($fnc20c->save()) return $this->FeekMsg('数据新增成功!',1);
            return $this->FeekMsg('数据新增失败!');
        }elseif ($mode == 'M'){
            $fnc20c = new Fnc0020c();
            /*
                if($data['command'] == config('setting.pwsd_mask')){
                    unset($data['command']);
                }else if(!empty($data['command'])){
                    $data = array_merge($data,
                        SvFaffair::getCmd($data['command'], $map['listid']));
                }
            */
            if($fnc20c->save($data,$map))  return $this->FeekMsg('数据更新成功!',1);
            return  $this->FeekMsg('数据更新失败!');
        }elseif ($mode == 'D'){
            $this->pushRptBack('fnc0020c',$map,'auto');
            return $this->FeekMsg('数据删除成功!');
        }
        return $this->FeekMsg('数据操作失败!');
    }
    // 获取数据列表
    public function sets(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $page = request()->param('page', 1);
        $num = request()->param('num', 12);
        $fnc20 = new Fnc0020c();
        $count = $fnc20->where('uid',$uid)->count();
        $subSql = (new Fnc1000c())->field('count(*)')->where('master_id = a.listid or slave_id = a.listid')->buildSql();
        $data = $fnc20->where('uid',$uid)
            ->field(['a.*',$subSql=>'set_count'])
            ->alias('a')
            ->order('a.type,a.mtime')
            ->page($page,$num)
            ->select();
        // 返回数据
        return $this->FeekMsg([
            'result' => $data,
            'page' => $page,
            'count'=> $count,
            'num'  => $num
        ]);
    }
    // 口令服务
    public function command(){
        $msg = '';
        // 图形验证码
        if(!captcha_check(trim(request()->param('code')))) $msg = '验证码无效';
        elseif (empty($listid = request()->param('listid'))){
            $msg = '请求参数不足!';
        }
        else{
            if(!\app\common\Aurora::checkUserPassw(
                request()->param('pwd'),
                (new \app\common\model\User())->getPassword()
            )){
                $msg = '用户密码无效!';
            }else{
                $type = request()->param('type');
                switch ($type){
                    case 'view':    // 口令查看
                        $data = SvFaffair::getRaw($listid);
                        return $this->FeekMsg($data);
                        break;
                    case 'change':  // 口令修改
                        $command = request()->param('command');
                        $data = SvFaffair::getCmd($command, $listid);
                        $fnc20c = new Fnc0020c();
                        if($fnc20c->save($data,['listid' => $listid]))  return $this->FeekMsg('口令更新成功!',1);
                        return  $this->FeekMsg('口令更新失败!');
                        break;
                }
            }

        }
        if($msg){
            return $this->FeekMsg($msg);
        }
    }
}