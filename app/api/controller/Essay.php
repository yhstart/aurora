<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/10/1 0001 23:51
 * Email: brximl@163.com
 * Name: 文章数据获取
 */

namespace app\api\controller;


use app\common\controller\Api;

class Essay extends Api
{
    /**
     * 获取文章列表
     */
    public function getList(){
        $request = $this->request;
        $num = $request->param('num', 20);
        $order = $request->param('order');
        switch ($order){
            case 'push_date':$order = 'mtime desc';break;
            case 'render':$order = 'read_count desc';break;
            case 'star':$order = 'star_count desc';break;
            default:$order = 'date desc';
        }
        $data = db()->table('atc1000c')
            ->field('collected,date,listid,title,read_count,star_count')
            ->where('is_private','N')
            ->order($order)
            ->limit($num)
            ->select();
        return $this->FeekMsg($data);
    }
}