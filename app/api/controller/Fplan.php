<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/22 0022 21:26
 * Email: brximl@163.com
 * Name:
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Fnc2000c;
use app\common\model\Fnc1000c;
use hyang\Util;
use think\Db;

class Fplan extends Api
{
    // 数据保存
    public function save(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        list($data,$mode,$map) = $this->_getSaveData('plan_no');
        $data = Util::dataClear($data,['start_date','end_date','cycle','pid','standard_money']);
        if(!isset($data['cycle']) && isset($data['cycle_unit'])) unset($data['cycle_unit']);
        $fnc = new Fnc2000c();
        Db::startTrans();
        try{
            if($mode == 'A') {
                $data['uid'] = $uid;
                $data['plan_no'] = $fnc->getNoVal();
                if($fnc->save($data)){
                    Db::commit();
                    return $this->FeekMsg('财务计划新增成功!',1);
                }
                return $this->FeekMsg('财务计划新增失败了！');
            }elseif ($mode == 'M'){
                if(!empty($map) && $fnc->save($data,$map)){
                    Db::commit();
                    return $this->FeekMsg('财务计划更新成功!',1);
                }
                return $this->FeekMsg('财务计划更新失败了！');
            }elseif ($mode == 'D'){
                if($this->pushRptBack($fnc->getTable(),$map,'auto')){
                    Db::commit();
                    return $this->FeekMsg('财务计划删除成功!',1);
                }
                return $this->FeekMsg('财务计划删除失败了！');
            }
        }catch (\Exception $e){
            debugOut($e->getMessage()."\r\n".$e->getTraceAsString());
            Db::rollback();
            return $this->FeekMsg('系统出错!');
        }
        //debugOut($data);
        return $this->FeekMsg('系统请求无效!');
    }
    // 获取登录用户的数据记录
    public function sets(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $fnc = new Fnc2000c();
        $page = request()->param('page');
        $num = request()->param('num');
        $page = is_numeric($page)? intval($page): 1;
        $num = is_numeric($num)? intval($num): 20;
        $subSql = (new Fnc1000c())->field('count(*)')->where('src_plan_no = a.plan_no')->buildSql();
        $data = $fnc
            ->alias('a')
            ->field(['a.*',$subSql=>'ctt'])
            ->where('a.uid',$uid)
            ->page($page, $num)
            ->select()
        ;
        $count = $fnc
            ->alias('a')
            ->where('a.uid',$uid)
            ->count()
            ;
        return $this->FeekMsg([
            'result' => $data,
            'page' => $page,
            'count'=> $count
        ]);
    }
}