<?php
/**
 * 请求前置处理
 * Created by PhpStorm.
 * User: Joshua Conero
 * Date: 2019/7/29
 * Time: 23:12
 * Email: conero@163.com
 */

namespace app\common\service;


use app\common\model\User;
use think\facade\Request;

class RequestBeforeCheck
{
    static $msg;
    static $msgWaitSec = 3600;
    static $url;

    /**
     * 入口检测，可中断请求
     * @return bool
     */
    static function enter(){
        self::user_verify();
        $breakOff = false;
        $only_site_user = config('app.only_site_user');
        $uid = getUserInfo('uid');
        if($only_site_user && empty($uid)){
            $ctrl = Request::controller();
            $mod = Request::module();
            $action = Request::action();
            $ignore = $only_site_user['ignore'] ?? [];
            $needLogined = true;
            $path = strtolower($mod . '/' . $ctrl . '/' . $action);
            foreach ($ignore as $ig){
                if(stripos($ig, $path) !== false){
                    $needLogined = false;
                    break;
                }
            }

            if($needLogined) {
                self::$msg = '十分抱歉，由于该网站资源有限！目前暂时关闭非登录用户的访问；此外我们的网站近期受到大量的爬虫，导致网站相应速度十分缓慢。期待您的慷慨解囊，以及网站正在继续维护中……';
                self::$url = isMobile()? url('/wap/login') : url('/index/login');
                $breakOff = true;
            }
        }
        return $breakOff;
    }

    /**
     * 用户认证
     */
    static function user_verify(){
        $verify = config('app.verify');
        $uid = getUserInfo('uid');
        if($verify && isset($verify['open']) && $verify['open'] && $uid){
            $user = User::get($uid);
            if($user){
                $useInfo = $user->toArray();
                $now = date('Y-m-d H:i:s');

                $uValid = true;
                $status = ($useInfo['status'] ?? null);
                // 首次需要进行验证
                if(empty($useInfo['last_lgtime'] ?? null)) $uValid = false;
                // 没有过期则判断有限期判断
                if($uValid && $status != User::StatusCodeExpire){
                    $dt1 = date_create($useInfo['last_lgtime']);
                    $dt2 = date_create('now');
                    $dt = date_diff($dt1, $dt2);
                    $dt = (int)($dt->format('%a'));
                    $expire_in = $verify['expire_in'] ?? 0;
                    if($expire_in>0 && $dt > $expire_in){
                        $user->save([
                            'status'=> User::StatusCodeExpire,
                            'last_lgtime' => $now
                        ], ['uid' => $uid]);
                        $uValid = false;
                    }
                }

                if($uValid && in_array($status, [User::StatusCodeNonVerify, User::StatusCodeExpire])) $uValid = false;
                if(!$uValid){
                    $ctrl = Request::controller();
                    $mod = Request::module();
                    $action = Request::action();
                    if(('index' == $mod || 'wap' == $mod) && 'verify' == strtolower($ctrl) && 'index' == $action){
                        return;
                    }
                    if(IS_MOBILE){
                        go('/wap/verify');
                    }else{
                        go('/index/verify');
                    }
                    exit();
                }
                // 正常时修改用户的登录信息
                $user->save([
                    'last_lgtime' => $now
                ], ['uid' => $uid]);
            }
        }
    }
}