<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/22 0022 21:33
 * Email: brximl@163.com
 * Name: 财务计划-模型
 */

namespace app\common\model;


use think\Model;

class Fnc2000c extends Model
{
    protected $table = 'fnc2000c';
    protected $pk = 'plan_no';
    // 获取主键
    public function getNoVal(){
        return getPkValue('pk_fnc2000c__plan_no');
    }

    /**
     * 计划自动生成
     * @param $item string|array
     * @param null $callback function ->($value,$type)
     * @return array
     */
    public function schedule($item,$callback=null,$ListCount=10){
        $data = is_array($item)? $item:
            $this->db()->where($this->pk,$item)->find()->toArray();
        $i = 0;
        // 如何从起始日期中记录一天与当前时间最近的一天，而非从过期的历史数据起始
        $startDate = empty($data['start_date'])? date('Y-m-d'):$data['start_date'];
        $result = ['source'=>$data];
        $content = [];
        if(!empty($data['cycle'])){
            // 月份
            if('M' == $data['cycle_unit']){
                $tplArray = explode('-',$startDate);
                $tmpD = $tplArray[2];
                $tmpY = intval($tplArray[0]);
                $tmpM = intval($tplArray[1]);
                while($i<$ListCount){
                    // 年份增加
                    if($tmpM == 12){
                        $tmpM = 0;
                        $tmpY = $tmpY + 1;
                    }
                    $tmpM = $tmpM + 1;
                    $value = $tmpY.'-'.str_pad($tmpM,2,'0',STR_PAD_LEFT).'-'.$tmpD;
                    if($callback instanceof \Closure){
                        call_user_func($callback,$value,$data['cycle_unit']);
                    }else
                        $content[] = $value;
                    $i++;
                }

            }
            // 年计划
            elseif ('Y' == $data['cycle_unit']){
                $tmpArray = explode('-',$startDate);
                $tmpY = intval($tmpArray[0]);
                $tmpMD = str_replace("$tmpY-",'',$startDate);
                while ($i<$ListCount){
                    $tmpY += 1;
                    $value = $tmpY.'-'.$tmpMD;
                    if($callback instanceof \Closure){
                        call_user_func($callback,$value,$data['cycle_unit']);
                    }else
                        $content[] = $value;
                    $i++;
                }
            }
            // 日计划
            else{
                $dtdays = intval($data['cycle']);
                $dtdays = is_int($dtdays)? $dtdays:1;
                while ($i<$ListCount) {
                    $date = date_create($startDate);
                    date_add($date, date_interval_create_from_date_string("$dtdays days"));
                    $startDate = date_format($date, 'Y-m-d');
                    if($callback instanceof \Closure){
                        call_user_func($callback,$startDate,$data['cycle_unit']);
                    }else
                        $content[] = $startDate;
                    $i++;
                }
            }
        }
        if(!empty($content)) $result['content'] = $content;
        return $result;
    }
}