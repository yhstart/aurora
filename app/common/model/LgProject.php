<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 13:01
 * Email: brximl@163.com
 * Name:
 */

namespace app\common\model;


use think\Model;

class LgProject extends Model
{
    protected $table = 'log_project';
    protected $pk = 'listid';
    // 关联对象
    public function Records(){
        return $this->hasMany('LgRecord', 'p_listid');
    }
    /**
     * 获取类型别名
     * @param string $t
     * @return string
     */
    public static function getTypeAlias($t){
        $map = [
            'D1' => '__day__',
            'M1' => '__month__',
            'Y1' => '__year__'
        ];
        $tmp = '';
        if(strpos($t, '_') === 0){
            $tmpArr = array_flip($map);
            if(isset($tmpArr[$t])) $tmp = $tmpArr[$t];
        }else{
            if(isset($map[$t])) $tmp = $map[$t];
        }
    }
}