<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 13:23
 * Email: brximl@163.com
 * Name: 日志明细记录
 */

namespace app\common\model;


use think\Model;

class LgRecord extends Model
{
    protected $table = 'log_record';
    protected $pk = 'no';

    // 获取主键
    public function getNoVal(){
        return getPkValue('pk_log_record__no');
    }
}