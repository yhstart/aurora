<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/1 0001 22:06
 * Email: brximl@163.com
 * Name: 系统角色
 */

namespace app\common\model;


use think\Model;

class Role extends Model
{
    protected $pk = "listid";
    /**
     * 获取所有用户角色
     * @param string $groupCode
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getRoles($groupCode='sys'){
        $uid = getUserInfo('uid');
        $roles = false;
        if($uid){
            $roles = $this->db()
                ->table('sys_role_member')
                ->alias('a')
                ->join($this->getTable().' b', 'a.role_id=b.listid')
                ->join((new Group())->getTable().' c', 'b.pid=c.listid')
                ->field('b.code')
                ->where(['c.code'=>$groupCode, 'a.user_uid'=>$uid])
                ->select()
                ;
            $arr = [];
            foreach ($roles as $v){
                $arr[] = $v['code'];
            }
            $roles = $arr;
        }
        return $roles;
    }
}