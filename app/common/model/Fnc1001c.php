<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/7/2 0002 23:14
 * Email: brximl@163.com
 * Name: 财务登账明细
 */

namespace app\common\model;


use think\Model;

class Fnc1001c extends Model
{
    protected $table = 'fnc1001c';
    protected $pk = 'no';
    const PkEngin = 'pk_fnc1001c__no';
    // 获取主键
    public function getNoVal(){
        return getPkValue(self::PkEngin);
    }
}