<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/25 0025 18:30
 * Email: brximl@163.com
 * Name: 桌面端首页-公共页面
 */

namespace app\common\traits;


use hyang\Dataset;
use think\facade\Request;

trait WebIndex
{
    /**
     * 页面模板
     * @param $callback
     * @return mixed
     */
    protected function pageTpl($callback){
        // 视图
        $this->autoRecordVisitRecord();
        $dataset = new Dataset();
        call_user_func($callback,$dataset);
        // 页面渲染
        $this->assign('pageContent', view(
            ROOT_PATH.'app/index/view/'. strtolower(Request::controller()).'/'.Request::action().'.html', $dataset->getSets())
            ->getContent()
        );
        // 用户信息
        $userInfo = getUserInfo();
        $this->assign('_user',$userInfo);
        return $this->fetch(ROOT_PATH. 'app/index/view/webindex.html');
    }
}