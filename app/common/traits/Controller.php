<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/5/7 0007 12:23
 * Email: brximl@163.com
 * Name: 控制器扩展
 */

namespace app\common\traits;

use app\common\SCache;
use hyang\Util as HyangUtil;
use think\facade\Config;
trait Controller
{
    use Util;
    protected $_JsVarOption = [];
    //  js/css 第一个 "/" 开头时不自动加载模块名
    //  加载前端工具 2016年9月21日 星期三   {auth: 权限标识,afterAuthFn:function 授权完成以后自定义脚本,title:页面标题,require:利用 FrontBuild 加载脚本, beforeLoadFront:string/function 加载前端脚本以前,js:js 脚本,css: css 脚本,afterLoadFront: string/function 加载前端页面以后,more:headplus,bootstrap: true 开启}
    // 回调函数时自动传入 $this 对象
    protected function loadScript($opt,$feek=false){
        // 权限检测
        if(isset($opt['auth'])){
            // 权限控制
        }
        //  未加载到 view对象时无效
        if(!method_exists($this,'assign')) return false;
        $AppAssignData = [];
        //  标题
        if(isset($opt['title'])){
            $AppAssignData['title'] = $opt['title'];
        }
        $script = '';
        if(isset($opt['afterAuthFn']) && ($opt['afterAuthFn'] instanceof \Closure)) $script .= call_user_func($opt['afterAuthFn'],$this);

        // 自定义脚本 - 前端载入以前
        if(isset($opt['beforeLoadFront'])){
            $script .= ($opt['beforeLoadFront'] instanceof \Closure)? call_user_func($opt['beforeLoadFront'],$this) : $opt['beforeLoadFront'];
        }
        $Pref = Config::get('setting.static_pref');
        //  js
        if(isset($opt['js']) && $opt['js']){
            $dir = $Pref.request()->module().'/js/';
            $js = is_array($opt['js'])? $opt['js']:array($opt['js']);
            foreach($js as $v){
                // 接收array参数 ： ['src'=>'js', 'nocache'=>bool]
                $nocache = false;
                if(is_array($v)){
                    if(isset($v['nocache'])) $nocache = true;
                    $v = isset($v['src'])? $v['src']: '';
                }
                if(strpos($v,'/') === 0) $jsSrc = $Pref.substr($v,1);
                else $jsSrc = $dir.$v;
                // 调试模式清除js缓存
                $randMk = Config::get('app_debug') && !$nocache? '?rd='.HyangUtil::randStr(10):'';
                $script .= '<script src="'.$jsSrc.'.js'.$randMk.'"></script>';
            }
        }
        //  css
        if(isset($opt['css']) && $opt['css']){
            $dir = $Pref.request()->module().'/css/';
            $js = is_array($opt['css'])? $opt['css']:array($opt['css']);
            foreach($js as $v){
                if(strpos($v,'/') === 0) $cssHref = $Pref.substr($v,1);
                else $cssHref = $dir.$v;
                // 调试模式清除css缓存
                $randMk = Config::get('app_debug')? '?rd='.HyangUtil::randStr(10):'';
                $script .= '<link rel="stylesheet" href="'.$cssHref.'.css'.$randMk.'" />';
            }
        }
        // 自定义脚本 - 前端载入以后
        if(isset($opt['afterLoadFront'])){
            $script .= ($opt['afterLoadFront'] instanceof \Closure)? call_user_func($opt['afterLoadFront'],$this) : $opt['afterLoadFront'];
        }
        if($script) $AppAssignData['web_front'] = $script;
        if($feek) return $AppAssignData;//生成 HTML
        $this->assign('app',$AppAssignData);
    }
    // PHP 后端数据传递到服务端
    protected function _JsVar($key=null,$value=null)
    {
        if(is_array($key)){
            $data = $this->_JsVarOption;
            $this->_JsVarOption = count($data)? array_merge($data,$key):$key;
            return true;
        }
        elseif(is_null($key)) return $this->_JsVarOption;
        elseif($value && $key){
            $this->_JsVarOption[$key] = $value;
            return true;
        }
        elseif(is_null($value)){
            if(array_key_exists($key,$this->_JsVarOption)) return $this->_JsVarOption[$key];
        }
        return '';

    }
    /**
     * // 重写
     * 加载模板输出
     * @access protected
     * @param string $template 模板文件名
     * @param array  $vars     模板输出变量
     * @param array  $replace  模板替换
     * @param array  $config   模板参数
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $replace = [], $config = [])
    {
        $this->beforeFetch();
        return $this->view->fetch($template, $vars, $replace, $config);
    }

    /**
     * 模板加载以前
     * @param \think\View|null $view
     */
    private function beforeFetch($view=null){
        $data = $this->_JsVarOption;
        if(is_array($data) && !empty($data)){
            $xhtml = '
                <script>
                    var AuroarJs = \''.(base64_encode(json_encode($data))).'\';
                </script>
            ';

            if($view){
                $view->assign('app_head_script',$xhtml);
            }else{
                $this->assign('app_head_script',$xhtml);
            }
        }
    }
    /**
     * 获取用户信息数据
     * @param null $key
     * @return array|mixed|string
     */
    protected function getUserInfo($key=null){
        return getUserInfo($key);
    }

    /**
     * api 认证注册
     */
    protected function apiCheckKeys(){
        $scache = new SCache();
        $key = Config::get('setting.sckey_name');
        if(!$scache->has($key,'Y')){
            $scache->set($key,'Y');
        }
    }
    /**
     * 自动分析分析出dtl-sumy 形式数据
     */
    protected function _getDtlData(){
        $data = request()->param();
        if(isset($data['sumy'])) $data['sumy'] = is_string($data['sumy'])? json_decode($data['sumy'],true):$data['sumy'];
        if(isset($data['dtl'])) $data['dtl'] = is_string($data['dtl'])? json_decode($data['dtl'],true):$data['dtl'];
        return $data;
    }

    /**
     * @param string $key
     * @param null|callable $callback
     * @return array|bool
     */
    protected function _baseWhereParse($key='args', $callback=null){
        $where = [];
        $args = isset($_POST[$key])? $_POST[$key]: false;
        /**
        //args => {col: 键值, value: 值, equal: 等式, type: 类型}
        args => {c: 键值, v: 值, e: 等式, t:类型}
         **/
        if($args && is_array($args)){
            $t = isset($args['t'])? $args['t']:false;
            if($t){
                if($callback && is_callable($callback)){
                    // &$where
                    call_user_func($callback, $where);
                }
                // 条件查询
            }else if(isset($args['e'])){
                $e = $args['e'];
                $v = $args['v'];
                $c = $args['c'];
                switch ($e){
                    case 'like':$where[] = [$c, 'like', "%$v%"];break;
                    case 'begin':$where[] = [$c, 'like', "%$v"];break;
                    case 'end':$where[] = [$c, 'like', "$v%"];break;
                    default:
                        $where[$c] = $v;
                }
            }
        }
        return empty($where)? false: $where;
    }
}