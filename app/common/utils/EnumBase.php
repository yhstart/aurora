<?php
/**
 * Created by PhpStorm.
 * User: Joshua Conero
 * Date: 2019/8/15
 * Time: 23:34
 * Email: conero@163.com
 */

namespace app\common\utils;


class EnumBase
{
    // 反馈信息列表
    const FeekSuccessCode = 1;
    const FeekFailureCode = -1;
}