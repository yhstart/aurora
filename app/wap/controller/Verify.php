<?php
/**
 * [移动版] 用户认证
 * User: Joshua Conero
 * Date: 2019/8/12
 * Time: 22:23
 * Email: conero@163.com
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\User;

class Verify extends Wap
{
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '用户认证',
            //'css'   => 'verify/index',
            'js'   => 'verify/index'
        ]);
        $data = User::get($this->getUserInfo('uid'));
        $data = $data -> toArray();
        $this->assign('data', $data);
        return $this->fetch();
    }
}