<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/17 0017 8:29
 * Email: brximl@163.com
 * Name: 标签管理
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc0010c;

class Ftag extends Wap
{
    // 主页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '标签管理',
            'js'    => ['/lib/zepto/touch','ftag/index']
        ]);
        return $this->fetch();
    }
    // 编辑页
    public function edit(){
        return $this->fetch();
    }
}