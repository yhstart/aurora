<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 9:47
 * Email: brximl@163.com
 * Name: 日志系统
 */

namespace app\wap\controller;


use app\common\controller\Wap;

class Log extends Wap
{
    // 首页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '日志系统',
            'js' => 'log/index',
            'css' => 'log/index',
        ]);
        return $this->fetch();
    }
}