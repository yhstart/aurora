<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 9:57
 * Email: brximl@163.com
 * Name:
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\LgProject as LgProjectMd;
use hyang\Bootstrap;

class Lgproject extends Wap
{
    // 首页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '日志',
            'js'    => 'lgproject/index'
        ]);
        return $this->fetch();
    }
    // 编辑界面
    public function edit(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '日志-编辑',
            'js'    => 'lgproject/edit'
        ]);
        $item = $this->request->param('item');
        if($item){
            $data = LgProjectMd::where('listid', $item)->find();
            $this->assign('data', $data);
            $this->assign('f_pk_grid',Bootstrap::formPkGrid($data,'listid'));
        }
        return $this->fetch();
    }
}