<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/21 0021 21:27
 * Email: brximl@163.com
 * Name:
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc0030c;

class Fsubject extends Wap
{
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '科目管理',
            'js'    => ['/lib/zepto/touch','fsubject/index']
        ]);
        return $this->fetch();
    }
}