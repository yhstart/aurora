<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/23 0023 22:54
 * Email: brximl@163.com
 * Name: 财务纪事
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc2001c;
use hyang\Bootstrap;

class Fevent extends Wap
{
    // 首页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '财务纪事',
            'js'    => ['fevent/index']
        ]);
        return $this->fetch();
    }
    // 编辑页面
    public function edit(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '编辑 | 财务纪事',
            'js'    => ['/lib/zepto/touch','fevent/edit']
        ]);
        $item = request()->param('item');
        $uid = $this->getUserInfo('uid');
        if($item){
            $fnc = new Fnc2001c();
            $data = $fnc->get($item)->toArray();
            if($uid != $data['uid']) $this->getErrorUrl();
            $this->assign('data',$data);
            $this->assign('f_pk_grid',Bootstrap::formPkGrid($data,'listid'));
        }
        return $this->fetch();
    }
}