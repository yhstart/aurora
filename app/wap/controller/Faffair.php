<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/13 0013 22:02
 * Email: brximl@163.com
 * Name: 事务甲乙方
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc0020c;
use app\common\model\Fnc1000c;
use hyang\Bootstrap;
use hyang\Util;

class Faffair extends Wap
{
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '甲乙事务 | 财务管理',
            'js'    => ['/lib/zepto/touch','faffair/index']
        ]);
        return $this->fetch();
    }
    // 编辑页面
    public function edit(){
        $this->checkAuth();
        $this->loadScript([
            'js' => ['/lib/zepto/touch','faffair/edit']
        ]);
        $listid = request()->param('uid');
        $uid = $this->getUserInfo('uid');
        $fnc20x = new Fnc0020c();
        $select = null;$checkd=null;
        $emode = 'A';
        if($listid){
            $data = $fnc20x->get($listid)->toArray();
            $data['command'] = config('setting.pwsd_mask');
            if($data['uid'] != $uid){
                $this->getErrorUrl('请求参数有误!');
            }
            $this->assign('data',$data);
            $select = $data['type'];
            if($data['use_mk'] == 'N') $checkd = true;
            $this->assign('pk_form',Bootstrap::formPkGrid($data));
            $emode = 'M';
        }
        //修改，使用mysql模型：only_full_group_by
        // 选取最近设置的十个
        $qdata = $fnc20x->where('uid',$uid)
            ->where('group_mk','not null')
            ->field('group_mk')
            //->order('mtime desc')
            ->group('group_mk, uid')
            ->limit(10)
            ->select();

        $groups = [];
        foreach ($qdata as $v){
            $groups[] = $v['group_mk'];
        }
        $page = ['emode'=>$emode];
        if($groups){
            $this->_JsVar('groups',$groups);
            $page['group_sel_able'] = 'Y';
        }
        $typeSel = Bootstrap::SelectGrid([
            'M0' => '事务甲方',
            'S0' => '事务乙方',
            '00' => '未区分',
        ],$select);
        // <input id="usemk_ipter" class="weui-switch-cp__input" name="use_mk" value="Y" type="checkbox" checked="checked">
        $page['use_mk'] = Bootstrap::RadioGrid('id="usemk_ipter" class="weui-switch-cp__input" name="use_mk" value="N"',
            $checkd, 'checkbox');
        $page['typeSel'] = $typeSel;
        // 辅助操作
        $this->assign('page',$page);
        return $this->fetch();
    }
}