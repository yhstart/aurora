<?php
/**
 * User: Joshua Conero
 * Date: 2017/5/6 0006 23:28
 * Email: brximl@163.com
 * Name: 用户登录
 */

namespace app\wap\controller;
use app\common\controller\Wap;
use app\common\model\User;
use think\facade\Config;
use think\Db;
use think\facade\Session;

class Login extends Wap
{
    public function index(){
        // 必须未登录用户
        $uid = getUserInfo('uid');
        if($uid){
            $this->getErrorUrl('您已经登录系统了，无需重复登录!');
        }
        $this->loadScript([
            'title' => '用户登录',
            'js'    => ['login/index']
        ]);
        return $this->fetch();
    }
}