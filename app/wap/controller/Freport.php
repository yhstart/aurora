<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/23 0023 23:12
 * Email: brximl@163.com
 * Name:
 */

namespace app\wap\controller;


use app\common\controller\Wap;

class Freport extends Wap
{
    // 首页
    public function index(){
        $dataList = '';
        $dataList = $dataList? $dataList:'
            <div class="weui-cell weui-cell_access">
                <div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>
                <div class="weui-cell__ft">
                    您还没一条财务计划记录
                </div>
            </div>
        ';
        $page = [];
        $page['list'] = $dataList;
        $this->assign('page',$page);
        return $this->fetch();
    }
    //@ 财务账单动态分析： 月度， 年度， 季度等
    public function dynamic(){
        $this->loadScript([
            'title' => '财务报表 | 报文动态生成',
            'js'    => ['freport/dynamic']
        ]);
        return $this->fetch();
    }
}