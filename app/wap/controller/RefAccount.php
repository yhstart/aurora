<?php
/**
 * Created by PhpStorm.
 * User: Joshua Conero
 * Date: 2019/9/4
 * Time: 22:12
 * Email: conero@163.com
 */

namespace app\wap\controller;


use app\common\controller\Wap;

class RefAccount extends Wap
{
    function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '参数账单',
            'js'    => ['ref_account/index'],
            'css'   => '/lib/filepond/filepond.min'
        ]);
        return $this->fetch();
    }
}