<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/12 0012 15:08
 * Email: brximl@163.com
 * Name: 日志记录表
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\LgProject;
use app\common\model\LgRecord as LgRecordMd;
use hyang\Bootstrap;

class Lgrecord extends Wap
{
    // 日志编辑
    public function edit(){
        $this->checkAuth();
        $item = $this->request->param('item');
        if(empty($item)) $this->getErrorUrl('请求参数不足');
        $logp = LgProject::where('listid', $item)->find();
        if(empty($logp)) $this->getErrorUrl('请求参数无效');
        $this->loadScript([
            'title' => '日志-'.($logp['name']).'-编辑',
            'js'    => [
                // textboxio 不在地址后面添加随机码
                ['src'=>'/lib/textboxio/textboxio', 'nocache'=>true],
                'lgrecord/edit'
            ]
        ]);
        $no = $this->request->param('no');
        $data = ['date'=>date('Y-m-d')];
        if($no){
            $data = LgRecordMd::where('no', $no)->find();
            $this->assign('pk_grid',Bootstrap::formPkGrid($data,'no'));
        }
        $data['p_listid'] = $logp['listid'];
        $this->assign('data', $data);
        return $this->fetch();
    }
    // 日志详情
    public function detail(){
        $this->checkAuth();
        $item = $this->request->param('item');
        if(empty($item)) $this->getErrorUrl('请求参数不足');
        $logp = LgProject::where('listid', $item)->find();
        if(empty($logp)) $this->getErrorUrl('请求参数无效');
        $this->loadScript([
            'title' => '日志-'.($logp['name']).'-编辑',
            'js'    => [
                ['src'=>'/lib/textboxio/textboxio', 'nocache'=>true],
                'lgrecord/detail'
            ],
            'css'   => 'lgrecord/detail'
        ]);
        $data = [
            'addUrl' => url('lgrecord/edit', 'item='.$logp['listid'])
        ];
        $logpArr = $logp->toArray();
        $data = array_merge($logpArr, $data);
        $this->assign('data', $data);
        $this->_JsVar('sumy', $logpArr);
        return $this->fetch();
    }
}