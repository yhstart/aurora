<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/10 0010 23:46
 * Email: brximl@163.com
 * Name: 财务账单
 */

namespace app\wap\controller;


use app\common\Aurora;
use app\common\controller\Wap;
use app\common\model\Fnc0030c;
use app\common\model\Fnc1000c;
use app\common\model\Fnc1001c;
use hyang\Bootstrap;
use hyang\Util;

class Faccount extends Wap
{
    //@ 首页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '财务账单',
            'js'    => ['/lib/zepto/touch','faccount/index']
        ]);
        $page = [];
        $fnc = new Fnc1000c();
        $page['count'] = $fnc->where('uid',$this->getUserInfo('uid'))->count();
        //$page['list'] = $dataList;
        $this->assign('page',$page);
        return $this->fetch();
    }
    // 记账
    public function edit(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '记账',
            'js'    => ['/lib/zepto/touch','faccount/edit']
        ]);
        $data = ['date'=>date('Y-m-d')];
        $uid = $this->getUserInfo('uid');
        $item = request()->param('item');
        $tpl = request()->param('tpl');
        $fnc = null;
        if($item){
            $fnc = new Fnc1000c();
            $data = $fnc->getOneList($item);
            // 只有本人才查看或编辑自己的账单
            if($uid != $data['uid']) $this->getErrorUrl('您的请求参数有误');
            $data['pre_url'] = urlBuild('!.faccount/preview/item/'.$data['no']);
            $this->assign('pk_grid',Bootstrap::formPkGrid($data,'no'));
        }elseif ($tpl){ // 模板新增
            $fnc = new Fnc1000c();
            $rdata = $fnc->getOneList($tpl);
            if($uid != $rdata['uid']) $this->getErrorUrl('您的请求参数有误');
            $data = Util::dataUnset($rdata,['date','money']);
            $data['ref_date'] = $rdata['date'];
            $data['ref_money'] = $rdata['money'];
            $refMsg = '<a href="'.urlBuild('!.faccount/preview/item/'.$rdata['no']).'">您正在参考： 《'.$data['ref_date'].
                '/'.$rdata['money'].'/'.$rdata['name'].'》登账.</a>';
            $data['ref_msg'] = $refMsg;
            $data['date'] = date('Y-m-d');
        }
        // 类型自动生成
        $type = isset($data['type'])? $data['type']:'OU';
        $data['type_opts'] = Bootstrap::SelectGrid(['IN'=>'收入','OU'=>'支出'],$type);
        // 顶部返回栏
        $data['page_head'] = $item?
            '<a href="'.urlBuild('!.faccount').'">
                <i class="fa fa-arrow-circle-left"></i>
                财务账单
            </a>
            /编辑
            '
            :
            '<a href="'.urlBuild('!.finance').'">
                <i class="fa fa-arrow-circle-left"></i>
                财务系统
            </a>
            /记账
           '
        ;
        /*
         // 通过ip分析的所在位置不准确，通常定位到服务器所在地
        $city = Aurora::visitSession('city');
        if(empty($city)){
            $location = Aurora::location();
            if(isset($location['code']) && empty($location['code'])){
                $city = $location['data']['city'];
                Aurora::visitSession('city',$city);
            }
        }
        */
        // 2018年2月22日 星期四
        if(empty($data['city'])){
            $city = Fnc1000c::where([
                'uid' => $uid
            ])
                ->where('city is not null')
                ->order('date desc')
                ->value('city')
                ;
            $data['city'] = $city;
        }
        $sbjct = new Fnc0030c();
        $data['subject_opts'] = Bootstrap::SelectGrid(function() use ($sbjct){
            $tmpData = $sbjct
                ->field('listid,subject')
                ->limit(10)
                ->select()
                ;
            $retVal = [];
            foreach ($tmpData as $v){
                $retVal[$v['listid']] = $v['subject'];
            }
            return $retVal;
        },(isset($data['subject_id'])? $data['subject_id']:null));

        //$data['subject_opts'] = Bootstrap::SelectGrid(function(){return ['l'=>'5555'];});
        $this->assign('data',$data);
        if($fnc){
            $this->_JsVar('tag_list', $fnc->getTags());
        }

        return $this->fetch();
    }
    // 财务明细
    public function detail(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '财务账单 | 明细',
            'js'    => ['/lib/zepto/touch','faccount/detail']
        ]);
        $item = request()->param('item');
        if(empty($item)) $this->getErrorUrl('财务账单明细请求参数不足或无效');
        $fnc = new Fnc1000c();
        $sum = $fnc->getOneList($item);
        if($sum['uid'] != $this->getUserInfo('uid')) $this->getErrorUrl('非法请求参数！');
        $sum['descrip'] = nl2br($sum['descrip']);
        $fnc2 = new Fnc1001c();
        $data = $fnc2
            ->where('src_no',$item)
            ->order('mtime asc')
            ->select();
        $listXhtml = '';
        if($data){
            $sum['detailAll'] = $fnc2
                ->where('src_no',$item)
                ->value('sum(money * part)')
            ;
        }
        foreach ($data as $v){
            $listXhtml .= '
            <a class="weui-cell weui-cell_access" href="javascript:;" data-no="'.$v['no'].'">
                <div class="weui-cell__bd">
                    <p>'.$v['name'].'</p>
                </div>
                <div class="weui-cell__ft"><span style="color:#FF6633;">'.(floatval($v['money']) * intval($v['part'])).'</span> ('.$v['money'].' * '.$v['part'].')</div>
            </a>
            ';
        }
        $this->assign('sum',$sum);
        $this->assign('list',$listXhtml);
        return $this->fetch();
    }
    // 预览
    public function preview(){
        $this->checkAuth();
        $item = request()->param('item');
        if(empty($item)) $this->getErrorUrl('财务账单明细请求参数不足或无效');
        $fnc = new Fnc1000c();
        $sum = $fnc->getOneList($item, true);
        if($sum['uid'] != $this->getUserInfo('uid')) $this->getErrorUrl('非法请求参数！');
        $this->loadScript([
            'title' => '财务账单 | 详情'.($sum['name']? '-'.$sum['name']:''),
            'js'    => ['/lib/zepto/touch'/*,'faccount/preview'*/]
        ]);
        $url = [
            'edit' => urlBuild('!.faccount/edit/item/'.$sum['no']),
            'addTpl' => urlBuild('!.faccount/edit/', ['__get'=>['tpl'=>$sum['no']]]),
            'detail' => urlBuild('!.faccount/detail/item/'.$sum['no']),
        ];
        $this->assign('sum',$sum);
        $this->assign('url',$url);
        return $this->fetch();
    }
}