<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/12/30 0030 10:01
 * Email: brximl@163.com
 * Name: 数据统计分析
 */

namespace app\index\controller;


use app\common\controller\Web;
use app\common\traits\WebIndex;

class Data extends Web
{
    use WebIndex;
    public function index(){
        $this->loadScript([
            'title' => '数据统计分析',
            'js'    => ['/lib/echart/echarts.min', 'data/index'],
            'css'    => ['data/index'],
        ]);
        return $this->pageTpl(function ($view){
            /** @var $view \think\View */
        });
    }
}