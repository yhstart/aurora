<?php
/**
 * [PC 版本] 用户认证
 * User: Joshua Conero
 * Date: 2019/8/12
 * Time: 22:24
 * Email: conero@163.com
 */

namespace app\index\controller;


use app\common\controller\Web;

class Verify extends Web
{
    public function index(){
        echo 'web版用户认证';
        return $this->fetch();
    }
}