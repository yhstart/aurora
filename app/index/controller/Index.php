<?php
/**
 * Auther: Joshua Conero
 * Email: brximl@163.com
 * Name: web 端网站首页
 */
namespace app\index\controller;
use app\common\controller\Web;
use app\common\model\Visit;
use app\common\traits\WebIndex;
use think\facade\Config;
use think\Db;

class Index extends Web
{
    use WebIndex;
    public function index()
    {
        $user = $this->getUserInfo('user');
        if($user){
            $this->_JsVar('user', $user);
        }
        $this->loadScript([
            'js' => ['/lib/echart/echarts.min','index/index'],
            'css'=> ['index/index']
        ]);
        return $this->pageTpl(function ($view) use ($user){
            /** @var $view think\facade\View*/
            $page = [];
            // 访问分布
            $rdata = (new Visit())->getVisitCount();
            $page['rate_wctt'] = ceil(($rdata['wcount']/$rdata['count'])*100);
            $page['rate_mctt'] = ceil(($rdata['mcount']/$rdata['count'])*100);
            // 全部统计量
            $page['count'] = $rdata['count'];
            $page['dcount'] = $rdata['dcount'];
            $oldt = Config::get('setting.online_date');
            $page['online_cttdt'] = getDays(date('Y-m-d'),$oldt);
            $page['online_dt'] = $oldt;
            $page['login_mk'] = $user? 'Y':'N';
            $view->assign('page',$page);
            // 数据
            $data = [];
            $count = Db::table('atc1000c')
                ->where('is_private','N')
                ->count();
            $data['article_count'] = $count;
            $view->assign('data',$data);
        });
    }
}
