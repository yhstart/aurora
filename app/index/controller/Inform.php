<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/10/2 0002 0:59
 * Email: brximl@163.com
 * Name: 系统信息
 */

namespace app\index\controller;


use app\common\controller\Web;
use app\common\model\Prj1002c;
use app\common\SCache;

class Inform extends Web
{
    public function read(){
        $item = request()->param('item');
        $prj2 = new Prj1002c();
        $data = $prj2->get($item)->toArray();

        $scache = new SCache();
        $count = $data['read_count'];
        if($scache->has('wap_inform_read_ctt',$item) == false){
            $count = $count + 1;
            $data['read_count'] = $count;
            $prj2->save(['read_count'=>$count],['listid'=>$item]);
            $scache->set('wap_inform_read_ctt',$item);
        }
        $this->assign('data',$data);
        return $this->fetch();
    }
}