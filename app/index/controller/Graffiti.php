<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/19 0019 23:07
 * Email: brximl@163.com
 * Name:
 */

namespace app\index\controller;


use app\common\controller\Web;
use app\common\traits\WebIndex;

class Graffiti extends Web
{
    use WebIndex;
    public function index(){
        $this->loadScript([
            'title' => '涂鸦',
            'js'    => ['graffiti/index'],
            'css'   => ['graffiti/index']
        ]);
        return $this->pageTpl(function ($view){});
    }
}