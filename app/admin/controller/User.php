<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/25 0025 10:32
 * Email: brximl@163.com
 * Name: 系统用户管理
 */

namespace app\admin\controller;


use app\common\controller\Web;
use app\common\traits\Admin;
use hyang\Bootstrap;
use app\common\model\User as UserModel;
use think\Db;

class User extends Web
{
    use Admin;
    protected $page_setting = [];
    protected function init()
    {
        $this->page_setting = $this->getParamFromMenu('user');
    }
    // @首页
    public function index(){
        return $this->pageTpl(function($view){
            $setting = $this->page_setting;
            $bstp = new Bootstrap();
            // 搜索栏
            $view->assign('searchBar',$bstp->searchBar([
                'account'        =>'登录名称',
                'name'           =>'姓名',
                'gender'         =>'性别',
                'register_time' =>'注册时间',
                'register_ip'   =>'注册ip'
            ]));
            $user = new UserModel();
            $where = $bstp->getWhere(null,['_col_'=>'a']);
            $count = $user->alias('a')
                ->where($where)
                ->count()
                ;
            $tbody = $bstp->tbodyGrid(['account','name','gender','login_count', 'register_time','register_ip'],
                function () use($user,$where, $bstp){
                    $subSql = Db::table('sys_login')
                        ->where('uid=a.uid')
                        ->field('count(*)')
                        ->select(false)
                        ;
                    return $user->alias('a')
                        ->field('a.*')
                        ->field(["($subSql)"=>'login_count', 'if(a.gender=\'F\', \'女\',\'男\') as gender'])
                        ->where($where)
                        ->page($bstp->page_decode(),30)
                        ->order('register_time desc')
                        ->select();
            });
            if($tbody){
                $view->assign('tbody',$tbody);
                $view->assign('pageBar',$bstp->pageBar($count));
            }
            $view->assign('setting',$setting);
        });
    }
}